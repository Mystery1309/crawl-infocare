# Created by ductn.93 at 05/10/22
from idhs.common.model.base_object import BeanContentData, BeanType, SpecialFields, GenderType, CentralType


class NewsSpecialFields(SpecialFields):
    def __init__(self, parent_category, sub_category, author, comment_count):
        self.parent_category = parent_category
        self.sub_category = sub_category
        self.author = author
        self.comment_count = comment_count

    def get_serializable_dict(self):
        return self.get_dict()

    def get_dict(self):
        return {
            "parent_category": self.parent_category,
            "sub_category": self.sub_category,
            "author": self.author,
            "comment_count": self.comment_count,

        }


class NewsBeanContent(BeanContentData):

    def __init__(self, bean_type: BeanType, download_date, full_contents: list, publish_date, title,
                 description, url,
                 attachment_urls: list,domain, special_fields: NewsSpecialFields, origin, source_id, source_name,):
        super().__init__(bean_type=bean_type, download_date=download_date,
                         full_contents=full_contents,
                         publish_date=publish_date, title=title, description=description, url=url,
                         attachment_urls=attachment_urls, domain=domain, special_fields=special_fields, origin=origin,
                         source_id=source_id, source_name=source_name, post_author_region="Việt Nam", post_author_gender=GenderType.Undefined,
                         post_author_age=None, post_author_central=CentralType.Other, post_author_location=None, hastags=[])

    def get_serializable_dict(self):
        return super().get_serializable_dict()
