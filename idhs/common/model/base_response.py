# /
# @created 2023/02/14 - 14:21
# @author ductn
# /

class BaseResponse(object):

    def __init__(self, postId, dataType, title, link, content, authorId, authorType,
                  nameSource, commentCount, shareCount, likeCount, publishDate, createdDate):
        self.postId = postId
        self.dataType = dataType
        self.title = title
        self.link = link
        self.content = content
        self.nameSource = nameSource
        self.commentCount = commentCount
        self.shareCount = shareCount
        self.likeCount = likeCount
        self.publishDate = publishDate
        self.createdDate = createdDate
        self.authorId = authorId
        self.authorType = authorType

    def get_serializable_dict(self):
        """
        Get a serializable dict of the instance of this class.
        :return:
        """
        tmp = self.get_dict()
        tmp["publishDate"] = str(tmp["publishDate"].strftime("%Y-%m-%dT%H:%M:%SZ"))
        tmp["createdDate"] = str(tmp["createdDate"].strftime("%Y-%m-%dT%H:%M:%SZ"))
        return tmp

    def get_dict(self):
        """
        Get the dict of the instance of this class.
        :return:
        """

        return {
            "postId": self.postId,
            "dataType": self.dataType,
            "link": self.link,
            "content": self.content,
            "title": self.title,
            "share_count": self.shareCount,
            "nameSource": self.nameSource,
            "commentCount": self.commentCount,
            "likeCount": self.likeCount,
            "publishDate": self.publishDate,
            "createdDate": self.createdDate,
            "authorType": self.authorType,
            "authorId": self.authorId
        }

class FacebookGroupCommentCare(object):
    def __init__(self, commentId, postId, contentComment, authorName, authorLink,
                  reactionCount, replyCount, publishDate, downloadDate, linkComment,
                 authorGender, authorType, authorId):
        self.commentId = commentId
        self.postId = postId
        self.contentComment = contentComment
        self.authorName = authorName
        self.authorLink = authorLink
        self.reactionCount = reactionCount
        self.replyCount = replyCount
        self.publishDate = publishDate
        self.downloadDate = downloadDate
        self.linkComment = linkComment
        self.authorGender = authorGender
        self.authorType = authorType
        self.authorId = authorId

    def get_serializable_dict(self):
        """
        Get a serializable dict of the instance of this class.
        :return:
        """
        tmp = self.get_dict()
        tmp["publishDate"] = str(tmp["publishDate"].strftime("%Y-%m-%dT%H:%M:%SZ"))
        tmp["downloadDate"] = str(tmp["downloadDate"].strftime("%Y-%m-%dT%H:%M:%SZ"))
        return tmp

    def get_dict(self):
        """
        Get the dict of the instance of this class.
        :return:
        """

        return {
            "commentId": self.commentId,
            "postId": self.postId,
            "authorName": self.authorName,
            "contentComment": self.contentComment,
            "authorLink": self.authorLink,
            "replyCount": self.replyCount,
            "reactionCount": self.reactionCount,
            "linkComment": self.linkComment,
            "publishDate": self.publishDate,
            "downloadDate": self.downloadDate,
            "authorGender": self.authorGender,
            "authorType": self.authorType,
            "authorId": self.authorId
        }
