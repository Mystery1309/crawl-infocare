# Created by ductn.93 at 05/10/22
import enum


class FullContent:
    def __init__(self, id, content, type):
        self.id = id
        self.content = content
        self.type = type

    def get_serializable_dict(self):
        return self.get_dict()

    def setUid(self, id):
        self.id = id

    def setContent(self, content):
        self.content = content

    def get_dict(self):
        return {
            "id": self.id,
            "content": self.content,
            "type": self.type
        }


class ProfileFacebook:
    def __init__(self, fid, name):
        self.fid = fid
        self.name = name

    def get_serializable_dict(self):
        return self.get_dict()

    def get_dict(self):
        return {
            "fid": self.fid,
            "name": self.name
        }


class AttachmentUrl:
    def __init__(self, id, url, type):
        self.id = id
        self.url = url
        self.type = type

    def setUid(self, id):
        self.id = id

    def setUrl(self, url):
        self.url = url

    def setType(self, type):
        self.type = type

    def get_serializable_dict(self):
        return self.get_dict()

    def get_dict(self):
        return {
            "id": self.id,
            "url": self.url,
            "type": self.type
        }


class SourceType(enum.Enum):
    page = enum.auto(),
    group_private = enum.auto(),
    profile = enum.auto(),
    follow_campaign = enum.auto(),
    group_public = enum.auto()


class BeanType(enum.Enum):
    bean_content_news = enum.auto(),

    bean_content_news_comment = enum.auto(),

    bean_content_facebook_post = enum.auto(),

    bean_content_facebook_comment = enum.auto(),

    bean_content_youtube_post = enum.auto(),

    bean_content_youtube_comment = enum.auto(),

    bean_content_google = enum.auto(),

    bean_content_book = enum.auto(),

    bean_content_manga = enum.auto(),

    bean_others = enum.auto(),

    bean_content_forum_post = enum.auto(),

    bean_content_bing_search = enum.auto(),

    bean_content_yahoo_search = enum.auto(),

    bean_content_tiktok = enum.auto(),

    bean_content_tiktok_trending = enum.auto(),

    bean_content_twitter = enum.auto(),

    bean_content_facebook_hastag = enum.auto(),

    bean_content_facebook_link = enum.auto(),

    bean_content_instagram = enum.auto(),

    bean_content_forum_comment = enum.auto()


class CentralType(enum.Enum):

    Northern = enum.auto(),
    Southern= enum.auto(),
    Other= enum.auto(),
    Central= enum.auto()


class GenderType(enum.Enum):

    Male = enum.auto(),
    FeMale= enum.auto(),
    Other= enum.auto(),
    Undefined= enum.auto()


class SpecialFields(object):

    def get_dict(self):
        return {}

    def get_serializable_dict(self):
        return self.get_dict()


class BeanContentData(object):

    def __init__(self, bean_type: BeanType, download_date, full_contents: list, publish_date, title,
                 description, url,
                 attachment_urls: list, domain, special_fields: SpecialFields, origin, source_id, source_name,
                 post_author_gender: GenderType, post_author_location, post_author_central: CentralType,
                 post_author_region, post_author_age, hastags:list):
        self.bean_type = str(bean_type.name)
        self.download_date = download_date
        self.full_contents = full_contents
        self.publish_date = publish_date
        self.title = title
        self.description = description
        self.url = url
        self.attachment_urls = attachment_urls
        self.domain = domain
        self.special_fields = special_fields
        self.origin = origin
        self.source_id = source_id
        self.post_author_gender = str(post_author_gender.name)
        self.source_name = source_name
        self.post_author_location = post_author_location
        self.post_author_central = str(post_author_central.name)
        self.post_author_region = post_author_region
        self.post_author_age = post_author_age
        self.hastags = hastags

    def get_serializable_dict(self):
        """
        Get a serializable dict of the instance of this class.
        :return:
        """
        tmp = self.get_dict()
        tmp["download_date"] = str(tmp["download_date"].strftime("%Y-%m-%dT%H:%M:%SZ"))
        tmp["publish_date"] = str(tmp["publish_date"].strftime("%Y-%m-%dT%H:%M:%SZ"))
        return tmp

    def get_dict(self):
        """
        Get the dict of the instance of this class.
        :return:
        """

        return {
            "bean_type": self.bean_type,
            "download_date": self.download_date,
            "publish_date": self.publish_date,
            "full_contents": self.full_contents,
            "title": self.title,
            "description": self.description,
            "url": self.url,
            "attachment_urls": self.attachment_urls,
            "domain": self.domain,
            "special_fields": self.special_fields.get_serializable_dict(),
            "origin": self.origin,
            "source_id": self.source_id,
            "post_author_gender": self.post_author_gender,
            "post_author_location": self.post_author_location,
            "post_author_central": self.post_author_central,
            "post_author_region": self.post_author_region,
            "post_author_age": self.post_author_age,
            "source_name": self.source_name,
            "hastags": self.hastags
        }
