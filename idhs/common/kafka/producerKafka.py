# /
# @created 2023/03/31 - 16:37
# @author ductn
# /

from kafka import KafkaProducer
import json

FB_COMMENT_TOPIC= "fb-comment-topic.pro"
FB_POST_TOPIC= "fb-post-topic.pro"
BOOTSTRAP_SERVERS = "10.30.3.51:9092"

class MessageProducer:

    def __init__(self):
        self.producer = KafkaProducer(bootstrap_servers=BOOTSTRAP_SERVERS,
        value_serializer=lambda v: json.dumps(v).encode('utf-8'),
        acks='all',
        retries=3)

    def send_msg_fb_comment(self, msg):
        print("sending message fb comment...")
        try:
            future = self.producer.send(FB_COMMENT_TOPIC,msg)
            self.producer.flush()
            future.get(timeout=60)
            print("message sent successfully...")
            return {'status_code':200, 'error':None}
        except Exception as ex:
            return ex

    def send_msg_fb_group_post(self, msg):
        print("sending message fb post...")
        try:
            future = self.producer.send(FB_POST_TOPIC,msg)
            self.producer.flush()
            future.get(timeout=60)
            print("message sent successfully...")
            return {'status_code':200, 'error':None}
        except Exception as ex:
            return ex
