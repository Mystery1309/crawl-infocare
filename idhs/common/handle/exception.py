# /
# @created 2023/02/14 - 15:47
# @author ductn
# /
class CookieError(Exception):
   def __init__(self, message):
       self.message = message
       super().__init__(self.message)


class TokenYoutubeError(Exception):
    def __init__(self, message):
        self.message= message
        super().__init__(self.message)


class TokenGoogleError(Exception):
    def __init__(self, message):
        self.message= message
        super().__init__(self.message)


class TokenRepuError(Exception):
    def __init__(self, message):
        self.message= message
        super().__init__(self.message)
