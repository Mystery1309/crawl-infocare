# /
# @created 2023/03/24 - 12:00
# @author ductn
# /
import json
import requests
class OAuth(object):

    @staticmethod
    def get_dict_current_customer():
        try:
            with open("/opt/idhs_worker/config/token_reputa.json", "r") as file:

                data = file.read()
                object_data = json.loads(data)
                file.close()
        except:
            with open("/opt/idhs_worker/config/token_reputa.json", "w") as file:
                object_data = {"api_call_headers": None
                               }
                file.write(json.dumps(object_data))
                file.close()

        return object_data


    @staticmethod
    def get_token_infocare():

        data = {'grant_type': 'password', 'username': 'tonhuduc1997@gmail.com', 'password': '123456'}

        access_token_response = requests.post("https://api.infocare.vn/auth-user/oauth/token", data=data, verify=False, allow_redirects=False,
                                              auth=("clientId", "secret"))

        tokens = json.loads(access_token_response.content)
        return {'Authorization': 'Bearer ' + tokens['access_token']}


    @staticmethod
    def set_token_customer(object_data):
        with open("/opt/idhs_worker/config/token_reputa.json", "w") as file:
            file.write(json.dumps(object_data))
            file.close()


    @staticmethod
    def save_data_fb_group_care(datas):

        object_data = OAuth.get_dict_current_customer()

        try:
            if object_data['api_call_headers'] is None:
                object_data['api_call_headers'] = OAuth.get_token_infocare()
                OAuth.set_token_customer(object_data)

            api_call_creates_data = requests.post("https://api.infocare.vn/processor/api/v1/fb-group-data-source/creates", allow_redirects=False,
                                                 json=datas,
                                                 headers=object_data['api_call_headers'],
                                                 verify=False)

            if api_call_creates_data.status_code == 401:
                object_data['api_call_headers'] = OAuth.get_token_infocare()
                OAuth.set_token_customer(object_data)
                api_call_creates_data = requests.post("https://api.infocare.vn/processor/api/v1/fb-group-data-source/creates", allow_redirects=False,
                                                     json=datas,
                                                     headers=object_data['api_call_headers'],
                                                     verify=False)

        except Exception as e:
            print(e)
