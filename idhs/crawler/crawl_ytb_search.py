# /
# @created 2023/02/14 - 09:38
# @author ductn
# /

from datetime import datetime, timedelta

from idhs.common.model.base_object import FullContent, AttachmentUrl

from googleapiclient.discovery import build
from idhs.common.model.base_response import BaseResponse


DEVELOPER_KEY = "AIzaSyBsXB_xJmdn-ts9BRaC8vISvxeEvB8GwdQ"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

WORKER_FACEBOOK = 'worker-facebook'


def convert_data(items, keySearch, brand_type):

    datas = []

    for item in items:
        try:
            snippet = item['snippet']
            thumbnails = snippet['thumbnails']

            # Get time post video and time get data of crawler
            publish_date = datetime.strptime(snippet['publishedAt'], '%Y-%m-%dT%H:%M:%SZ') + timedelta(hours=7)

            # Get info of channel owner
            channel_id = snippet['channelId']
            channel_title = snippet['channelTitle']
            video_id = item['id']['videoId']
            thumbnails_url = thumbnails['medium']['url']

            # Convert url redirect to video
            url = "https://www.youtube.com/watch?v=" + video_id

            # Convert content ytb to list
            full_contents = []
            title = snippet['title']
            full_contents.append(FullContent(id=str(title).__hash__(), content=title, type="text").get_dict())
            description = snippet['description']
            full_contents.append(FullContent(id=str(description).__hash__(), content=description, type="text").get_dict())

            # Convert url image to list
            attachments_url = [
                AttachmentUrl(id=str(thumbnails_url).__hash__(), url=thumbnails_url, type="img").get_dict()]

            contentYtb = title + "\n"+ description

            save_data = BaseResponse(dataType="Youtube", content=contentYtb,
                                     publishDate=publish_date, title=title, postId=video_id, createdDate=datetime.now(),
                                     likeCount=0, shareCount=0,
                                     commentCount=0, link=url,
                                     nameSource=channel_title, authorId=channel_id,
                                     authorType="Ytb").get_serializable_dict()

            datas.append(save_data)
        except Exception as e:
            print(e)
            continue

    return datas


def run(keywords, youtube, brand_type, time_num, max_result):
    list_datas = []
    hours = datetime.now().hour
    publishedAfter = datetime.now() - timedelta(hours=hours, days=time_num)
    publishedAfterString = str(publishedAfter.strftime("%Y-%m-%dT%H:%M:%SZ"))
    for keyword in keywords:
        response = None
        try:
            request = youtube.search().list(
                part="snippet",
                maxResults=max_result,
                publishedAfter=publishedAfterString,
                q=f"{keyword}",
                type="video",
                order="date"
            )
            response = request.execute()
        except Exception as e:
            if e.status_code == 401:
                message = "disabled"
                return list_datas, message
            elif e.status_code == 403:
                message = "limit"
                return list_datas, message

        datetime.now()

        items = response['items']
        if len(items) == 0:
            continue

        # list_datas = convert_data(items)
        list_datas.extend(convert_data(items, keyword, brand_type))

    message = "Done"

    return list_datas, message

def executeRequest(token, keyword, brand_type, time_num, max_result):
    keywords = [f"{keyword}"]

    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=token)
    ytb_responses, notify = run(keywords, youtube, brand_type, time_num=time_num, max_result=max_result)

    return ytb_responses
