# /
# @created 2023/07/13 - 16:01
# @author ductn
# /

from datetime import datetime, timedelta
from vnstock import *

def execute(stockName, dayNumber):

    startDate = (datetime.now() - timedelta(days=dayNumber)).strftime('%Y-%m-%d')
    endDate = (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d')


    price_history = stock_historical_data(stockName, startDate, endDate, "1D")

    price_dicts = price_history.to_dict("records")

    list_stock_monitoring = []

    for price_dict in price_dicts:

        list_stock_monitoring.append({
          "open": price_dict['open'],
          "high": price_dict['high'],
          "low": price_dict['low'],
          "close": price_dict['close'],
          "volume": price_dict['volume'],
          "time": price_dict['time'].strftime('%Y-%m-%dT%H:%M:%SZ'),
          "stockName": stockName
        })

    return list_stock_monitoring
