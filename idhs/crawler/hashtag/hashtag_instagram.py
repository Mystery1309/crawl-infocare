# /
# @created 2023/05/30 - 09:36
# @author ductn
# /
import requests

def resolved_data(hashtag_name):

    url = f"https://www.instagram.com/explore/tags/{hashtag_name}/"

    response = requests.get(url=url)

    html_content = response.text
    pageID = html_content[html_content.rindex('page_id')+10:].split('"')[0]
    if pageID == hashtag_name:
        return hashtag_name

    return None


