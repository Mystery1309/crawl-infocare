# /
# @created 2023/05/30 - 13:58
# @author ductn
# /

import requests


def resolved_link(hashtag_name):
    url = f"https://www.youtube.com/hashtag/{hashtag_name}"

    response = requests.get(url)
    html_response = response.text
    try:
        hashtagInfoText = html_response.index('hashtagInfoText')
        return hashtag_name
    except:
        return None