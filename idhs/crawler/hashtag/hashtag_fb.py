# /
# @created 2023/05/30 - 09:36
# @author ductn
# /
import requests

def resolved_link(hashtag_name):

    url = f"https://www.facebook.com/hashtag/{hashtag_name}"

    response = requests.get(url=url)
    try:
        datas = response.text.index(f'#{hashtag_name}')
        return hashtag_name
    except:
        return None


