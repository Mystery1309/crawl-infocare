# /
# @created 2023/02/14 - 09:38
# @author ductn
# /

# /
# @created 2023/02/14 - 09:21
# @author ductn
# /

# /
# @created 2023/02/07 - 16:43
# @author ductn
# /

import json
import re
from datetime import datetime
from time import sleep

import requests

from idhs.common.model.base_object import *
from idhs.common.model.base_response import BaseResponse

def convert_content(content):
    p =re.compile(r'(<!--.*?-->|<[^>]*>)')
    content = p.sub('', content)
    full_content = []
    text_split = content.split("\n")
    for c in text_split:
        full_content.append(FullContent(id=str(c).__hash__(), content=c, type="text").get_dict())

    return full_content


def run(BODY_FORUM, headers, account_reputa, max_result):
    list_crawl = []
    list_url = []
    temp = 0
    headers["Authorization"] = f"VTCCSSO {account_reputa.get('token')}"
    for idx in range(100):
        BODY_FORUM['page'] = idx
        response_fb = requests.post(url="https://apidn.reputa.vn/console/news/preview-search", json=BODY_FORUM, headers=headers)
        sleep(0.5)
        if response_fb.status_code != 200:

            return list_crawl
        response_data_fb = json.loads(response_fb.text)
        data_fb = response_data_fb.get('data')
        hits_fb = data_fb.get("hits")
        if len(hits_fb) == 0:
            break
        list_crawl.extend(convert_data_fb(hits_fb, headers, account_reputa, list_url))
        if len(list_crawl) > temp:
            temp = len(list_crawl)
        else:
            break

        if len(list_crawl) >= max_result:
            break

    return list_crawl


def get_content(response):

    full_contents = convert_content(json.loads(response).get('data'))

    return full_contents

def convert_data_fb(hits_fb, headers, account_reputa, list_url):
    list_data = []
    for data in hits_fb:

        url = data.get("url")
        if url in list_url:
            continue
        list_url.append(url)
        contentPost = data['content']


        list_data.append({"url": url,
                          "content": contentPost})


    return list_data


def executeData(main_keywords, max_result):

    BODY_WEBSITE = {
        "size": 1000,
        "length": 1000,
        "included_keywords": [
            {
                "main_keywords": [f"{main_keywords}"
                ],
                "sub_keywords": []
            }
        ],
         "topic_source": {
            "mainstream_news": 1,
            "local_news": 1,
            "journal": 1,
            "high_traffic_news": 1,
            "medium_traffic_news": 1,
            "youtube_video_post": 1,
            "forum": 1
        }
    }


    headers = {'Connection': 'keep-alive', 'Cache-Control': 'max-age=0', 'viewport-width': '1680',
               'Upgrade-Insecure-Requests': '1',
               'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
               'Sec-Fetch-Site': 'none', 'Sec-Fetch-Mode': 'navigate', 'Sec-Fetch-User': '?1',
               'Sec-Fetch-Dest': 'document',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'}

    account_reputa = {
        "user": "user",
        'topic': 530660,
        'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJtYXJnYXJldHlvdW5nZTZsZXVrZHBAZ21haWwuY29tIiwiZXhwIjoxNjkxMzA1OTI5LCJpYXQiOjE2ODM1Mjk5MjksInVzZXJuYW1lIjoibWFyZ2FyZXR5b3VuZ2U2bGV1a2RwQGdtYWlsLmNvbSIsInJvbGUiOiJ1c2VyIiwib3JnYW5pemF0aW9uX2lkIjowLCJlbWFpbCI6Im1hcmdhcmV0eW91bmdlNmxldWtkcEBnbWFpbC5jb20iLCJ1c2VyX2lkIjoiMTU1MjYiLCJleHBpcmVkIjpmYWxzZX0.nn1tq8VpigZwkBCYFI61AVakeQXPvZprvcQGk5upmzU'
    }

    datas = run(BODY_FORUM=BODY_WEBSITE, headers=headers, account_reputa=account_reputa, max_result=max_result)
    print(len(datas))
    return datas


