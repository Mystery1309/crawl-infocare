# /
# @created 2023/02/23 - 10:00
# @author ductn
# /

from time import sleep
from datetime import datetime

from idhs.common.handle.exception import *
from idhs.common.model.base_object import FullContent
from bs4 import BeautifulSoup
import re
import json
import requests
import json5
from idhs.common.model.base_response import BaseResponse

variables_comment = dict()
variables_comment["before"] = None
variables_comment["displayCommentsFeedbackContext"] = None
variables_comment["displayCommentsContextEnableComment"] = None
variables_comment["displayCommentsContextIsAdPreview"] = None
variables_comment["displayCommentsContextIsAggregatedShare"] = None
variables_comment["displayCommentsContextIsStorySet"] = None
variables_comment["feedLocation"] = "PAGE_TIMELINE"
variables_comment["feedbackSource"] = 22
variables_comment["first"] = 50
variables_comment["focusCommentID"] = None
variables_comment["includeHighlightedComments"] = False
variables_comment["includeNestedComments"] = True
variables_comment["isInitialFetch"] = False
variables_comment["isPaginating"] = True
variables_comment["last"] = None
variables_comment["scale"] = 1
variables_comment["topLevelViewOption"] = None
variables_comment["useDefaultActor"] = False
variables_comment["viewOption"] = None
variables_comment["UFI2CommentsProvider_commentsKey"] = "CometSinglePageContentContainerFeedQuery"


def get_full_contents(content):
    full_contents = []
    try:
        full_content = FullContent(id=str(content).__hash__(), content=content, type="text")
        full_contents.append(full_content.get_dict())
    except Exception as e:
        print(e)

    return full_contents

def get_interactive(feedback_target_with_context, name_count):
    count = 0
    try:
        count = feedback_target_with_context.get('ufi_renderer').get('feedback').get(
            "comet_ufi_summary_and_actions_renderer").get(
            "feedback").get(name_count).get("count")
    except:
        pass

    return count


def get_comment_count(feedback_target_with_context):
    count_comment = 0
    try:
        count_comment = feedback_target_with_context.get('ufi_renderer').get('feedback').get('total_comment_count')
    except Exception as e:
        print(e)

    return count_comment


def handle_interactions(pool_reaction):
    count = 0
    try:
        count = pool_reaction.get("reaction_count")
    except:
        pass

    return count

def check_content(content, topics):
    responses = None
    for topic in topics:
        if topic.lower() in content.lower():
          if responses is None:
              responses = topic
          else:
              responses += ", "+topic

    return responses





def parser_data(data):
    save_data = None
    content = ""
    try:
        node_comet_sections = data.get("node").get("comet_sections")
        feedback_target_with_context = node_comet_sections.get("feedback").get("story").get("feedback_context").get(
            "feedback_target_with_context")
        story_comet_sections = node_comet_sections.get("context_layout").get("story").get("comet_sections")

        # TODO: get content of post
        try:
            try:
                content = node_comet_sections.get("content").get("story").get('comet_sections').get("message").get('story').get('message').get("text")
            except:
                content = node_comet_sections.get("content").get("story").get('comet_sections').get('attached_story').get('story').get('attached_story').get('comet_sections').get('attached_story_layout').get('story').get('message').get("text")
        except:
            pass
        title = re.split('[,.]', content)[0]

        # TODO: get reaction count of post
        interaction = {'LIKE': 0, 'LOVE': 0, 'SUPPORT': 0, 'HAHA': 0, 'WOW': 0, 'ANGER': 0, 'SORRY': 0,
                       "share_count": get_interactive(feedback_target_with_context, "share_count"),
                       "reaction_count": get_interactive(feedback_target_with_context, "reaction_count"),
                       "comment_count": get_comment_count(feedback_target_with_context)}

        # TODO: get post id
        post_id = data['node']['post_id']
        url = f"https://www.facebook.com/{post_id}"

        # TODO: get info of page
        page_name = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get("name")
        page_id = node_comet_sections.get("content").get("story").get("actors")[0].get("id")

        # TODO: get time public of post page
        try:
            publish_date_int = int(story_comet_sections.get("metadata")[0].get("story").get("creation_time"))
        except:
            publish_date_int = int(story_comet_sections.get("metadata")[1].get("story").get("creation_time"))
        publish_date = datetime.fromtimestamp(publish_date_int)

        # TODO: check map topic with content post of page
        save_data = BaseResponse(dataType="Facebook Group", content=content,
                                     publishDate=publish_date, title=title, postId=post_id, createdDate=datetime.now(),
                                     likeCount=interaction["reaction_count"], shareCount=interaction["share_count"],
                                     commentCount=interaction["comment_count"], link=url,
                                     nameSource=page_name, authorId=page_id, authorType="Page").get_serializable_dict()

    except Exception as e:
        print(e)

    return save_data


def get_token(response):
    response_index = response.index('"token":')
    index = response_index + 8
    response_string = response[index: index + 100]
    print(response_string)
    # Get token
    tokens = response_string.split('"')
    token = tokens[1]

    return token


def convert_doc_id(content_script):
    doc_id = None
    if "e.exports=" in content_script:
        doc_id = content_script[content_script.index("e.exports=")+11:].split('"')[0]

    return doc_id


def get_list_doc_string(script_list):
    list_doc_script = []
    for script_e in script_list:
        try:
            href = script_e.attrs.get("src")
            if href is None:
                continue
            if "https://static.xx.fbcdn.net/rsrc.php/v3" in href:
                list_doc_script.append(href)
        except:
            continue
    doc_id_post = None
    for doc_script in list_doc_script:
        response_comment = requests.get(doc_script)
        sleep(0.3)

        response_doc = response_comment.text.split("\n")
        for string in response_doc:
            if string.startswith('__d("CometModernPageFeedPaginationQuery_facebookRelayOperation"'):

                doc_id_post = convert_doc_id(string)
                break

        if doc_id_post is not None:
            return doc_id_post

    return doc_id_post


def get_data_post_first(parser_html):
    lis_data = []
    strings = []
    script_tag = parser_html.find_all('script')
    for script_text in script_tag:
        try:
            text = str(script_text.contents[0])
            if '"end_cursor"' in text and 'CometGHLFakeElementsWrapper' in text:
                    strings.append(text)

        except:
            continue

    script = strings[0]
    data_json = json.loads(script)["require"]
    _data = dict()
    for data in data_json[0][3][0]['__bbox']['require']:
        try:
            if 'RelayPrefetchedStreamCache' in data[0] and "adp_CometSinglePageContentContainerFeedQueryRelayPreloader" in data[3][0]:
                _data = data[3][1]
                lis_data = _data.get("__bbox").get("result").get("data").get("page").get("timeline_feed_units").get(
                    "edges")
                break
        except Exception as e:
            print(e)

    return _data, lis_data

def get_variable(_data, page_info):
    variables = _data.get("__bbox").get("variables")
    variables["cursor"] = page_info.get("end_cursor")
    variables["count"] = 3
    variables["id"] = variables.get("pageID")
    variables["focusCommentID"] = None
    del variables["pageID"]
    variable_string = json.dumps(variables)

    return variable_string


def get_data_response(token, variable_string, doc_id_post, headers, max_result):
    list_data_res = []
    form_data = dict()
    form_data["fb_dtsg"] = token
    form_data["variables"] = variable_string
    form_data["doc_id"] = doc_id_post
    count = 0
    variable_dict = json.loads(variable_string)
    cursor = None
    while count >= 0:
        if cursor is not None:
            variable_dict["cursor"] = cursor
            form_data["variables"] = json.dumps(variable_dict)
        response_api = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)
        sleep(1)

        parser_html_api = BeautifulSoup(response_api.text, "html.parser")
        class_no_js_api = parser_html_api.find_all("html", {"class": "no_js"})

        if not (response_api.status_code == 200 and len(class_no_js_api) == 0):
            break

        if response_api.text == "object":
            data_res = response_api.text
        else:
            try:
                data_res = json.loads(response_api.text.split("\n")[0])
            except:
                data_res = json5.loads(response_api.text)

        cursor = data_res.get("data").get("node").get("timeline_feed_units").get("page_info").get("end_cursor")

        list_data_res.append(data_res)
        count += 1
        if len(list_data_res) >= max_result:
            break

    return list_data_res


def convert_data_facebook(lis_path):
    list_data = []
    for data in lis_path:
        try:
            data_post = parser_data(data=data)
            if data_post is not None:
                list_data.append(data_post)
        except:
            continue

    return list_data


def extend_data_post_paths(data_res, list_data_first):
    list_path = []
    list_path.extend(list_data_first)
    for data in data_res:
        feedUnits = data.get("data").get("node").get("timeline_feed_units").get("edges")
        list_path.extend(feedUnits)

    return list_path


def check_response(response_request):

    parser_html = BeautifulSoup(response_request.text, "html.parser")
    if len(parser_html.find_all("html", {"class": "no_js"})):

        raise CookieError("Cookies error !")
    response = response_request.text

    return parser_html, response


def get_page_info(_data):
    page_info = _data.get("__bbox").get("result").get("data").get("page").get("timeline_feed_units").get(
        "page_info")

    return page_info


def run(link_page, headers, max_result):
    response_request = requests.get(link_page, headers=headers)
    sleep(1)
    parser_html, response = check_response(response_request)

    try:
        # TODO: get all script of html
        script_list = parser_html.find_all('script')

        # TODO: get doc id page
        doc_id_post = get_list_doc_string(script_list)

        # TODO: get value of fb token in payload get graphql
        token = get_token(response)

        # TODO: get first data of page
        _data, list_data_first = get_data_post_first(parser_html)
        page_info = get_page_info(_data)
        variable_string = get_variable(_data, page_info)
        data_res = get_data_response(token, variable_string, doc_id_post, headers, max_result)

        # TODO: get list block data of page
        lis_post_path = extend_data_post_paths(data_res, list_data_first)

        # TODO: convert list bock to dict
        return convert_data_facebook(lis_path=lis_post_path)
    except Exception as e:
        print(e)


def executeData(cookies_string, link_page, topics, brand_type, max_result):

    headers = {
        'Connection': 'keep-alive',
        'Cache-Control': 'max-age=0',
        'viewport-width': '1680',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-User': '?1',
        'Sec-Fetch-Dest': 'empty',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-ch-ua-mobile': '?0',
        'Cookie': f'{cookies_string}'
    }

    list_data = run(link_page=link_page, headers=headers, max_result=max_result)

    return list_data