# /
# @created 2023/02/14 - 09:38
# @author ductn
# /

import json
import re
from datetime import datetime

import json5
import requests

from bs4 import BeautifulSoup
from idhs.common.handle.exception import *

from idhs.common.model.base_object import FullContent, ProfileFacebook

from idhs.common.model.base_response import BaseResponse
def handle_interactions(pool_reaction):
    count = 0
    try:
        count = pool_reaction.get("reaction_count")
    except:
        pass

    return count


def get_interactive(feedback_target_with_context, name_count):
    count = 0
    try:
        count = feedback_target_with_context.get('ufi_renderer').get('feedback').get(
            "comet_ufi_summary_and_actions_renderer").get(
            "feedback").get(name_count).get("count")
    except:
        pass

    return count


def get_comment_count(feedback_target_with_context):
    count_comment = 0
    try:
        count_comment = feedback_target_with_context.get('ufi_renderer').get('feedback').get('total_comment_count')
    except Exception as e:
        print(e)

    return count_comment


def get_doc_id_search(script_tag):
    doc_id_search = None
    list_doc_script = []
    for script_e in script_tag:
        try:
            href = script_e.attrs.get("src")
            if href is None:
                continue
            if "https://static.xx.fbcdn.net/rsrc.php/v3i" in href:
                list_doc_script.append(href)
        except Exception as e:
            print(e)
            continue
    for doc in list_doc_script:
        response_script = requests.get(doc)
        if response_script.text == "":
            continue

        response_arr = response_script.text.split("\n")

        for string in response_arr:
            if string.startswith('__d("SearchCometResultsPaginatedResultsQuery_facebookRelayOperation"'):
                doc_id_string = string[string.index("e.exports") + 11:].split('"')[0]
                try:
                    doc_id_search = int(doc_id_string)
                    return doc_id_search
                except:
                    continue
    return doc_id_search


def get_object_group(edge, form_data, doc_id_group, headers, owner):
    list_data_group = []
    status_type = True
    try:
        header_model = edge.get('relay_rendering_strategy').get('view_model').get('header_model')
        group_id = header_model['author_model']['target']['id']

        story_id = edge.get('relay_rendering_strategy').get('view_model').get('click_model').get(
            'story').get(
            'id')
        variables_group = {"UFI2CommentsProvider_commentsKey": "CometGroupPermalinkRootFeedQuery",
                           "displayCommentsContextEnableComment": None,
                           "displayCommentsContextIsAdPreview": None,
                           "displayCommentsContextIsAggregatedShare": None,
                           "displayCommentsContextIsStorySet": None,
                           "displayCommentsFeedbackContext": None,
                           "feedLocation": "GROUP_PERMALINK",
                           "feedbackSource": 2,
                           "focusCommentID": None,
                           "groupID": f"{group_id}",
                           "privacySelectorRenderLocation": "COMET_STREAM",
                           "renderLocation": "group_permalink",
                           "scale": 1,
                           "storyID": f"{story_id}",
                           "useDefaultActor": False}

        form_data_group = dict()
        form_data_group['fb_dtsg'] = form_data['fb_dtsg']
        form_data_group['__comet_req'] = form_data['__comet_req']
        form_data_group['__a'] = form_data['__a']
        form_data_group['variables'] = json.dumps(variables_group)
        form_data_group['doc_id'] = str(doc_id_group)

        list_data_group = get_data_detail_group(form_data=form_data_group,from_object=owner,
                                                headers=headers)
    except Exception as e:
        print(e)
        status_type = False
    return list_data_group, status_type

def get_full_contents_special(message, node_comet_sections):
    full_contents = []
    message_shares = None
    try:
        try:
            message_shares = node_comet_sections.get('content').get('story').get('attached_story').get(
                'comet_sections').get('message').get('story').get('message').get("text")
        except:
            pass

        if message is not None:
            try:
                string = message.get('story').get('message').get("text")
            except:
                string = node_comet_sections['content']['story']['comet_sections']['message_container']['story']['message']['text']
            contents = string.split("\n")
            for content in contents:
                full_contents.append(FullContent(id=str(content).__hash__(), content=content, type="text").get_dict())

        if message_shares is not None:
            contents = message_shares.split("\n")
            for content in contents:
                if content == "":
                    continue
                full_contents.append(FullContent(id=str(content).__hash__(), content=content, type="text").get_dict())
    except:
        print("Error full_content facebook profile type post")

    return full_contents


def get_type_owner(type_owner):
    source = None
    if type_owner == "Page":
        source = "Facebook Fanpage"
    if type_owner == "User":
        source = "Facebook User"
    if source is None:
        source = "Facebook Group"

    return source


def convert_reactions_post(interaction, pool_reaction):
    localized_name = pool_reaction['node']['localized_name'].lower()
    reaction_count = pool_reaction['reaction_count']
    if localized_name =='Thích'.lower() or localized_name == 'Like'.lower():
        interaction['LIKE'] = reaction_count
    elif localized_name == 'Yêu thích'.lower() or localized_name == 'Love'.lower():
        interaction['LOVE'] = reaction_count
    elif localized_name == 'Haha'.lower():
        interaction['HAHA'] = reaction_count
    elif localized_name == 'Thương thương'.lower() or localized_name == 'Care'.lower():
        interaction['SUPPORT'] = reaction_count
    elif localized_name == 'Wow'.lower():
        interaction['WOW'] = reaction_count
    elif localized_name == 'Buồn'.lower() or localized_name == 'Sad'.lower():
        interaction['SORRY'] =  reaction_count
    elif localized_name == 'Phẫn nộ'.lower() or localized_name == "Angry".lower():
        interaction['ANGER'] = reaction_count

def convert_data_rendering(edge):
    list_data = []

    node_comet_sections = edge['relay_rendering_strategy']['view_model']['click_model']['story']['comet_sections']
    story_comet_sections_root = edge['relay_rendering_strategy']['view_model']['click_model']['story']
    feedback_target_with_context = node_comet_sections.get("feedback").get("story").get("feedback_context").get(
        "feedback_target_with_context")
    story_comet_sections = node_comet_sections.get("context_layout").get("story").get("comet_sections")
    content = node_comet_sections['content']['story']['comet_sections']['message']
    full_contents = get_full_contents_special(content, node_comet_sections)

    interaction = {'LIKE': 0, 'LOVE': 0, 'SUPPORT': 0, 'HAHA': 0, 'WOW': 0, 'ANGER': 0, 'SORRY': 0, 'share_count': get_interactive(feedback_target_with_context, "share_count"),
                   "reaction_count": get_interactive(feedback_target_with_context, "reaction_count"),
                   "comment_count": get_comment_count(feedback_target_with_context)}
    try:
        pool_reactions = feedback_target_with_context.get('ufi_renderer').get('feedback').get(
            "comet_ufi_summary_and_actions_renderer").get(
            "feedback").get("cannot_see_top_custom_reactions").get("top_reactions").get("edges")
        for pool_reaction in pool_reactions:
            convert_reactions_post(interaction, pool_reaction)
    except:
        print("Loi pool reaction")

    title = None
    if len(full_contents):
        title = re.split('[,.]', full_contents[0].get("content"))[0]

    url = node_comet_sections['content']['story']['wwwURL']

    type_owner = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get('__typename')
    source = get_type_owner(type_owner=type_owner)
    post_id = story_comet_sections_root['post_id']
    try:
        if 'groups' not in url:
            url = "https://www.facebook.com/" + post_id
        else:
            source = "Facebook Group"
    except:
        pass

    name_owner_post = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get("name")
    id_owner_post = node_comet_sections.get("content").get("story").get("actors")[0].get("id")
    try:
        publish_date_int = int(story_comet_sections.get("metadata")[0].get("story").get("creation_time"))
    except:
        publish_date_int = int(story_comet_sections.get("metadata")[1].get("story").get("creation_time"))
    publish_date = datetime.fromtimestamp(publish_date_int)

    save_data = BaseResponse(dataType=source, content=convertContentPost(full_contents),
                             publishDate=publish_date, title=title, postId=post_id, createdDate=datetime.now(),
                             likeCount=interaction["reaction_count"], shareCount=interaction["share_count"],
                             commentCount=interaction["comment_count"], link=url,
                             nameSource=name_owner_post, authorId=id_owner_post, authorType=type_owner).get_serializable_dict()

    list_data.append(save_data)

    return list_data


def get_object_profile(edge, form_data, doc_id_user, owner, headers):
    list_data_user = []
    try:
        story_id = edge.get('relay_rendering_strategy').get('view_model').get('click_model').get(
            'story').get(
            'id')
        variables_profile = {"UFI2CommentsProvider_commentsKey": "CometSinglePostRoute",
                             "displayCommentsContextEnableComment": None,
                             "displayCommentsContextIsAdPreview": None,
                             "displayCommentsContextIsAggregatedShare": None,
                             "displayCommentsContextIsStorySet": None,
                             "displayCommentsFeedbackContext": None,
                             "feedLocation": "PERMALINK",
                             "feedbackSource": 2,
                             "focusCommentID": None,
                             "privacySelectorRenderLocation": "COMET_STREAM",
                             "renderLocation": "permalink",
                             "scale": 1,
                             "storyID": f"{story_id}",
                             "useDefaultActor": False}
        form_data_profile = dict()
        form_data_profile['fb_dtsg'] = form_data['fb_dtsg']
        form_data_profile['__comet_req'] = form_data['__comet_req']
        form_data_profile['__a'] = form_data['__a']
        form_data_profile['variables'] = json.dumps(variables_profile)
        form_data_profile['doc_id'] = str(doc_id_user)

        list_data_user = get_data_detail_profile(form_data=form_data_profile,
                                                 to_object=owner, headers=headers)
    except Exception as e:
        print(e)

    return list_data_user


def check_error(class_no_api, account):
    status = True
    if len(class_no_api) > 0:
        status = False

    return status


def check_rate_limited(parser_html_api, account):
    status = True
    try:
        if parser_html_api is None or 'Rate limit exceeded' in json.loads(parser_html_api.text)['errors'][0]['message']:
            print('Rate limit exceeded')
            data_limit = dict()
            data_limit['account'] = account
            data_limit['limit'] = False
            status = False

    except:
        pass

    return status


def get_cursor(script_tag):
    scripts = []
    scripts_bsid = []
    for script_text in script_tag:
        try:
            text = script_text.contents[0].split("\n")
            if len(text) == 1 and ("FBInteractionTracingDependencies" in text[0] or '"end_cursor"' in text[0]):
                scripts.append(text[0])

        except:
            continue

    try:
        cursor = scripts[0][scripts[0].rindex("end_cursor") + 13:].split('"')[0]
    except:
        cursor = scripts[1][scripts[1].rindex("end_cursor") + 13:].split('"')[0]

    try:
        bsid = scripts[0][scripts[0].index("bsid") + 7:].split('"')[0]
    except:
        bsid = scripts[0][scripts[0].index("bsid") + 7:].split('"')[1]

    return cursor, bsid


def get_list_edge(count, variable, form_data, headers, account, max_result):
    cursor_data = None
    list_edges = []
    try:
        while count >= 0:
            # TODO: Check cursor first data
            if cursor_data is not None:
                variable['cursor'] = cursor_data
                form_data['variables'] = json.dumps(variable)
            response_api = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)
            try:
                data_response = json.loads(response_api.text)
            except:
                data_response = json.loads(response_api.text.split("\n")[0])
            parser_html_api = BeautifulSoup(response_api.text, "html.parser")
            status = check_rate_limited(parser_html_api=parser_html_api, account=account)
            if not status:
                raise CookieError("Rate Limited Token Facebook Search !")

            # TODO: check token checkpoint
            class_no_api = parser_html_api.find_all("html", {"class": "no_js"})
            status_error = check_error(class_no_api=class_no_api, account=account)
            if not status_error:
                raise CookieError("Cookie Error !")

            if response_api.status_code == 200 and len(class_no_api) == 0:
                list_edges.extend(data_response.get('data').get('serpResponse').get('results').get('edges'))
                cursor_data = data_response.get('data').get('serpResponse').get('results').get('page_info').get(
                    'end_cursor')
                if not data_response.get('data').get('serpResponse').get('results').get('page_info').get(
                        'has_next_page'):
                    break
                count += 1
            if len(list_edges) >= max_result:
                break
    except Exception as e:
        print(e)

    return list_edges


def get_cursor_group(script_tag):
    scripts = []
    for script_text in script_tag:
        try:
            text = script_text.contents[0].split("\n")
            if len(text) == 1 and '"bsid"' in text[0]:
                scripts.append(text[0])

        except:
            continue

    try:
        cursor = scripts[0][scripts[0].rindex("end_cursor") + 13:].split('"')[0]
        bsid = scripts[0][scripts[0].index("bsid") + 7:].split('"')[0]
    except:
        cursor = scripts[1][scripts[1].rindex("end_cursor") + 13:].split('"')[0]
        bsid = scripts[1][scripts[1].index("bsid") + 7:].split('"')[0]

    return cursor, bsid


def check_response(response_api):
    parser_html_api = BeautifulSoup(response_api.text, "html.parser")
    class_no_api = parser_html_api.find_all("html", {"class": "no_js"})
    if response_api.status_code == 200 and len(class_no_api) == 0:
        return True
    return False


def get_full_contents(content):
    full_contents = []
    try:

        full_content = FullContent(id=str(content).__hash__(), content=content, type="text")
        full_contents.append(full_content.get_dict())
    except Exception as e:
        print(e)

    return full_contents

def get_detail_post_page(edge, form_data, doc_id_page, headers):
    list_data = []
    author_dict = edge['relay_rendering_strategy']['view_model']['header_model']['author_model']['author']
    name_author = author_dict.get('name')
    id_author = author_dict.get('id')
    from_object = ProfileFacebook(fid=id_author, name=name_author).get_dict()
    story_ids = edge['relay_rendering_strategy']['view_model']['click_model']['story']['id']
    variables_page = {"UFI2CommentsProvider_commentsKey": "CometPagePostsRootHoistedStoryQuery",
                      "displayCommentsContextEnableComment": None,
                      "displayCommentsContextIsAdPreview": None,
                      "displayCommentsContextIsAggregatedShare": None,
                      "displayCommentsContextIsStorySet": None,
                      "displayCommentsFeedbackContext": None,
                      "feedLocation": "PAGE_TIMELINE_PERMALINK",
                      "feedbackSource": 2,
                      "focusCommentID": None,
                      "privacySelectorRenderLocation": "COMET_STREAM",
                      "renderLocation": "permalink",
                      "scale": 1,
                      "storyIDs": [f"{story_ids}"],
                      "useDefaultActor": False}

    form_data["variables"] = json.dumps(variables_page)

    form_data["doc_id"] = doc_id_page
    response_detail = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)

    if not check_response(response_detail):
        return list_data
    try:
        data_detail = json.loads(response_detail.text.split("\n")[0])
    except:
        data_detail = json5.loads(response_detail.text)

    node_comet_sections = data_detail.get('data').get("nodes")[0].get("comet_sections")
    feedback_target_with_context = node_comet_sections.get("feedback").get("story").get("feedback_context").get(
        "feedback_target_with_context")

    story_comet_sections = node_comet_sections.get("context_layout").get("story").get("comet_sections")
    content = \
        data_detail['data']['nodes'][0]['comet_sections']['content']['story']['comet_sections']['message']['story'][
            'message']['text']

    full_contents = get_full_contents(content)
    try:
        publish_date_int = int(story_comet_sections.get("metadata")[0].get("story").get("creation_time"))
    except:
        publish_date_int = int(story_comet_sections.get("metadata")[1].get("story").get("creation_time"))
    publish_date = datetime.fromtimestamp(publish_date_int)

    post_id = data_detail['data']['node']['post_id']
    url = f"https://www.facebook.com/{post_id}"
    interaction = {'LIKE': 0, 'LOVE': 0, 'SUPPORT': 0, 'HAHA': 0, 'WOW': 0, 'ANGER': 0, 'SORRY': 0,
                   "share_count": get_interactive(feedback_target_with_context, "share_count"),
                   "reaction_count": get_interactive(feedback_target_with_context, "reaction_count"),
                   "comment_count": get_comment_count(feedback_target_with_context)}

    title = None
    if len(full_contents):
        title = re.split('[,.]', full_contents[0].get("content"))[0]


    save_data = BaseResponse(dataType="Facebook Fanpage", content=convertContentPost(full_contents),
                             publishDate=publish_date, title=title, postId=post_id, createdDate=datetime.now(),
                             likeCount=interaction["reaction_count"], shareCount=interaction["share_count"],
                             commentCount=interaction["comment_count"], link=url,
                             nameSource=name_author, authorId=id_author, authorType="Page").get_serializable_dict()

    list_data.append(save_data)

    return list_data

def get_data_detail_group(form_data, from_object, headers):
    list_data = []
    response_detail = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)
    if not check_response(response_detail):
        return list_data

    try:
        data_detail = json.loads(response_detail.text.split("\n")[0])
    except:
        data_detail = json5.loads(response_detail.text)

    comet_sections = data_detail['data']['node']['comet_sections']
    try:
        content = comet_sections['content']['story']['comet_sections']['message_container']['story']['message']['text']
    except:
        content = comet_sections['content']['story']['comet_sections']['message']['story']['message']['text']
    feedback_target_with_context = comet_sections['feedback']['story']['feedback_context'][
        'feedback_target_with_context']

    story_comet_sections = comet_sections.get("context_layout").get("story").get("comet_sections")
    full_contents = []
    list_word = []
    try:
        list_word = re.split('[ \n]', content)
    except:
        pass
    try:
        share = comet_sections['content']['story']['comet_sections']['attached_story']['story']['attached_story'][
            'comet_sections']['attached_story_layout']['story']['comet_sections']['message_container']['story']
        content_share = share['message']['text']
        if content_share != "":
            full_contents.extend(get_full_contents(content_share))
            list_word.extend(re.split('[ \n]', content_share))
    except:
        pass
    full_contents.extend(get_full_contents(content))

    attachments = comet_sections['content']['story']['attachments']
    if len(attachments) > 0:

        if attachments[0]['style_type_renderer']['__typename'] == 'StoryAttachmentCommerceAttachmentStyleRenderer':
            try:
                content_share = attachments[0]['style_type_renderer']['attachment']['title_with_entities']['text']
                full_contents.extend(get_full_contents(content_share))
            except:
                pass

    interaction = {'LIKE': 0, 'LOVE': 0, 'SUPPORT': 0, 'HAHA': 0, 'WOW': 0, 'ANGER': 0, 'SORRY': 0,
                   "share_count": get_interactive(feedback_target_with_context, "share_count"),
                   "reaction_count": get_interactive(feedback_target_with_context, "reaction_count"),
                   "comment_count": get_comment_count(feedback_target_with_context)}

    title = None
    if len(full_contents):
        title = re.split('[,.]', full_contents[0].get("content"))[0]
    try:
        publish_date_int = int(story_comet_sections.get('metadata')[0].get("story").get("creation_time"))
    except:
        publish_date_int = int(story_comet_sections.get('metadata')[1].get("story").get("creation_time"))

    publish_date = datetime.fromtimestamp(publish_date_int)

    post_id = data_detail['data']['node']['post_id']
    url = f"https://www.facebook.com/{post_id}"


    save_data = BaseResponse(dataType="Facebook Group", content=convertContentPost(full_contents),
                             publishDate=publish_date, title=title, postId=post_id, createdDate=datetime.now(),
                             likeCount=interaction["reaction_count"], shareCount=interaction["share_count"],
                             commentCount=interaction["comment_count"], link=url,
                             nameSource=from_object['name'], authorId=from_object["fid"], authorType="User").get_serializable_dict()



    list_data.append(save_data)


    return list_data


def get_data_detail_profile(form_data, to_object, headers):
    list_data = []
    response_detail = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)
    if not check_response(response_detail):
        return list_data

    try:
        data_detail = json.loads(response_detail.text.split("\n")[0])
    except:
        data_detail = json5.loads(response_detail.text)

    full_contents = []
    list_word = []
    comet_sections = data_detail['data']['node']['comet_sections']
    try:
        content_share = \
            comet_sections['content']['story']['comet_sections']['attached_story']['story']['attached_story'][
                'comet_sections']['attached_story_layout']['story']['message']['text']
        attachments = comet_sections['content']['story']['comet_sections']['attached_story']['story']['attached_story'][
            'comet_sections']['attached_story_layout']['story']['attachments']
        if len(content_share) > 3:
            list_word.extend(re.split('[ \n]', content_share))
            full_contents.extend(get_full_contents(content_share))
            if len(attachments) > 0:
                    try:
                        content_data = attachments[0]['style_type_renderer']['attachment']['title_with_entities'][
                            'text']
                        list_word.extend(re.split('[ \n]', content_data))
                        full_contents.extend(get_full_contents(content_data))
                    except:
                        pass


    except:
        pass
    feedback_target_with_context = comet_sections.get("feedback").get("story").get("feedback_context").get(
        "feedback_target_with_context")
    content = None
    try:
        try:
            content = comet_sections['content']['story']['comet_sections']['message']['text']
        except:
            content = \
                comet_sections['content']['story']['comet_sections']['message_container']['story']['message']['story'][
                    'message']['text']
    except:
        if comet_sections['content']['story']['comet_sections']['message'] is not None:
            try:
                content = comet_sections['content']['story']['comet_sections']['message']['story']['message']['text']
            except:
                rich_messages = comet_sections['content']['story']['comet_sections']['message']['rich_message']
                for message in rich_messages:
                    full_contents.extend(get_full_contents(message['text']))
    if content is not None:
        list_word.extend(re.split('[ \n]', content))
        full_contents.extend(get_full_contents(content))
    attachments_post = comet_sections['content']['story']['attachments']

    if len(attachments_post) > 0:

        if attachments_post[0]['style_type_renderer']['__typename'] == 'StoryAttachmentCommerceAttachmentStyleRenderer':

            try:
                content_data = attachments_post[0]['style_type_renderer']['attachment']['title_with_entities']['text']
                full_contents.extend(get_full_contents(content_data))
            except:
                pass
        elif attachments_post[0]['style_type_renderer']['__typename'] == 'StoryAttachmentShareSevereStyleRenderer':
            try:
                content_data = attachments_post[0]['style_type_renderer']['attachment']['title_with_entities']['text']
                full_contents.extend(get_full_contents(content_data))
            except:
                pass

    interaction = {'LIKE': 0, 'LOVE': 0, 'SUPPORT': 0, 'HAHA': 0, 'WOW': 0, 'ANGER': 0, 'SORRY': 0,
                   "share_count": get_interactive(feedback_target_with_context, "share_count"),
                   "reaction_count": get_interactive(feedback_target_with_context, "reaction_count"),
                   "comment_count": get_comment_count(feedback_target_with_context)}

    title = None
    if len(full_contents):
        title = re.split('[,.]', full_contents[0].get("content"))[0]

    post_id = data_detail['data']['node']['post_id']
    url = f"https://www.facebook.com/{post_id}"

    story_comet_sections = comet_sections.get("context_layout").get("story").get("comet_sections")

    try:
        publish_date_int = int(story_comet_sections.get("metadata")[0].get("story").get("creation_time"))
    except:
        publish_date_int = int(story_comet_sections.get("metadata")[1].get("story").get("creation_time"))

    publish_date = datetime.fromtimestamp(publish_date_int)

    save_data = BaseResponse(dataType="Facebook User", content=convertContentPost(full_contents),
                             publishDate=publish_date, title=title, postId=post_id, createdDate=datetime.now(),
                             likeCount=interaction["reaction_count"], shareCount=interaction["share_count"],
                             commentCount=interaction["comment_count"], link=url,
                             nameSource=to_object['name'], authorId=to_object['fid'], authorType="User").get_serializable_dict()

    list_data.append(save_data)

    return list_data


def convertContentPost(full_contents):
    contentData = None
    for full_content in full_contents:
        if contentData is None:
            contentData = full_content['content']
        else:
            contentData += " \n"+full_content['content']

    return contentData

def convert_html_overview(keyword, headers, account):

    query_request = {
        "q": keyword,
        "filters": "eyJyZWNlbnRfcG9zdHM6MCI6IntcIm5hbWVcIjpcInJlY2VudF9wb3N0c1wiLFwiYXJnc1wiOlwiXCJ9IiwicnBfY3JlYXRpb25fdGltZTowIjoie1wibmFtZVwiOlwiY3JlYXRpb25fdGltZVwiLFwiYXJnc1wiOlwie1xcXCJzdGFydF95ZWFyXFxcIjpcXFwiMjAyM1xcXCIsXFxcInN0YXJ0X21vbnRoXFxcIjpcXFwiMjAyMy0xXFxcIixcXFwiZW5kX3llYXJcXFwiOlxcXCIyMDIzXFxcIixcXFwiZW5kX21vbnRoXFxcIjpcXFwiMjAyMy0xMlxcXCIsXFxcInN0YXJ0X2RheVxcXCI6XFxcIjIwMjMtMS0xXFxcIixcXFwiZW5kX2RheVxcXCI6XFxcIjIwMjMtMTItMzFcXFwifVwifSIsInJwX2F1dGhvcjowIjoie1wibmFtZVwiOlwibWVyZ2VkX3B1YmxpY19wb3N0c1wiLFwiYXJnc1wiOlwiXCJ9In0%3D"
    }
    response_search = requests.get(
        f"https://www.facebook.com/search/posts",
        headers=headers, params=query_request)
    parer_html = BeautifulSoup(response_search.text, 'html.parser')

    class_no_api = parer_html.find_all("html", {"class": "no_js"})
    status_error = check_error(class_no_api=class_no_api, account=account)
    if not status_error:
        raise CookieError("Cookie error !")

    return response_search.text, parer_html


def convert_payload_search(response, parer_html, account, keyword):

    # TODO: get token field of payload search
    response_index = response.index('"token":')
    index = response_index + 8
    response_string = response[index: index + 100]
    print(response_string)
    tokens = response_string.split('"')
    token = tokens[1]

    # TODO: get cursor of payload search
    script_tag = parer_html.find_all('script')
    cursor = None
    bsid = None
    try:
        cursor, bsid = get_cursor(script_tag)
    except:
        pass

    if cursor is None:
        check_rate_limited(None, account=account)
        raise CookieError("Account not get Cursor facebook search !")

    # TODO: get doc id search of payload search
    doc_id_search = get_doc_id_search(script_tag)

    # TODO: convert variable of payload search
    variable = {
          "UFI2CommentsProvider_commentsKey": "SearchCometResultsInitialResultsQuery",
          "allow_streaming": False,
          "args": {
            "callsite": "COMET_GLOBAL_SEARCH",
            "config": {
              "exact_match": False,
              "high_confidence_config": None,
              "intercept_config": None,
              "sts_disambiguation": None,
              "watch_config": None
            },
            "context": {
              "bsid": f"{bsid}",
              "tsid": None
            },
            "experience": {
              "encoded_server_defined_params": None,
              "fbid": None,
              "type": "POSTS_TAB"
            },
            "filters": [
              "{\"name\":\"creation_time\",\"args\":\"{\\\"start_year\\\":\\\"2023\\\",\\\"start_month\\\":\\\"2023-1\\\",\\\"end_year\\\":\\\"2023\\\",\\\"end_month\\\":\\\"2023-12\\\",\\\"start_day\\\":\\\"2023-1-1\\\",\\\"end_day\\\":\\\"2023-12-31\\\"}\"}",
              "{\"name\":\"merged_public_posts\",\"args\":\"\"}",
              "{\"name\":\"recent_posts\",\"args\":\"\"}"
            ],
            "text": f"{keyword}"
          },
          "count": 5,
          "cursor": f"{cursor}",
          "displayCommentsContextEnableComment": False,
          "displayCommentsContextIsAdPreview": False,
          "displayCommentsContextIsAggregatedShare": False,
          "displayCommentsContextIsStorySet": False,
          "displayCommentsFeedbackContext": None,
          "feedLocation": "SEARCH",
          "feedbackSource": 23,
          "fetch_filters": True,
          "focusCommentID": None,
          "locale": None,
          "privacySelectorRenderLocation": "COMET_STREAM",
          "renderLocation": "search_results_page",
          "scale": 1,
          "stream_initial_count": 0,
          "useDefaultActor": False,
          "__relay_internal__pv__SearchCometResultsShowUserAvailabilityrelayprovider": True,
          "__relay_internal__pv__IsWorkUserrelayprovider": False,
          "__relay_internal__pv__StoriesRingrelayprovider": False,
          "__relay_internal__pv__FBReelsScopedSearchEnableHoverAndPlayrelayprovider": False
        }

    # TODO: generate payload search
    form_data = dict()
    form_data["fb_dtsg"] = token
    form_data["variables"] = json.dumps(variable)
    form_data["doc_id"] = str(doc_id_search)
    form_data["__comet_req"] = 1
    form_data["__a"] = 1

    return form_data, variable


def run(keyword, headers, account, max_result):
    response, parer_html = convert_html_overview(keyword=keyword, headers=headers, account=account)

    form_data, variable = convert_payload_search(response=response, parer_html=parer_html, account=account, keyword=keyword)

    # TODO: get list block data
    count = 0
    list_edges = get_list_edge(count=count, variable=variable, form_data=form_data, headers=headers, account=account, max_result=max_result)

    list_data = []
    list_link_post = []

    # TODO: get doc id of page, group, profile
    doc_id_page, doc_id_group, doc_id_user = 4528553017164756, 4362559363782361, 4802803696411031

    for edge in list_edges:
        try:
            type_name = edge['relay_rendering_strategy']['__typename']
            if type_name == 'SearchRichPostRenderingStrategy':
                list_data.extend(convert_data_rendering(edge))
                try:
                    list_link_post.append(edge['relay_rendering_strategy']['view_model']['click_model']['story']['comet_sections']['content']['story']['wwwURL'])
                except:
                    pass
            if type_name == 'SearchPostRenderingStrategy':
                link_post = edge['relay_rendering_strategy']['view_model']['click_model']['permalink']
                list_link_post.append(link_post)
                type = edge.get('relay_rendering_strategy').get('view_model').get('header_model').get(
                    'author_model').get(
                    'author').get('__typename')

                # TODO: get data by type page
                if type == 'Page':
                    form_data_page = dict()
                    form_data_page['fb_dtsg'] = form_data['fb_dtsg']
                    form_data_page['__comet_req'] = form_data['__comet_req']
                    form_data_page['__a'] = form_data['__a']
                    list_data_page = get_detail_post_page(edge=edge, form_data=form_data_page,
                                                          doc_id_page=doc_id_page,
                                                          headers=headers)
                    list_data.extend(list_data_page)
                    continue
                if type == 'User':

                    # TODO: get data by type group public
                    author_dict = edge.get('relay_rendering_strategy').get('view_model').get('header_model').get(
                        'author_model').get('author')
                    owner_name_post = author_dict['name']
                    owner_id_post = author_dict['id']
                    owner = ProfileFacebook(fid=owner_id_post, name=owner_name_post).get_dict()
                    data_detail, status_type = get_object_group(edge=edge, form_data=form_data,
                                                                doc_id_group=doc_id_group,
                                                                headers=headers, owner=owner
                                                                )
                    # TODO: get data by type profile
                    if not status_type:
                        data_detail = get_object_profile(edge=edge, form_data=form_data, doc_id_user=doc_id_user,
                                                        owner=owner, headers=headers)

                    list_data.extend(data_detail)
        except Exception as e:
            print(e)
            continue


    return list_data

def executeRequest(cookies_string, keyword, max_result):
    account = "100067888780508"
    # cookies_string = "sb=dLnSYDWeHw74yjCI4P1uXdkp; wd=1920x976; datr=dLnSYD-opIrhrAY1Z7p2Hgem; c_user=100068609987249; xs=40%3AqSL67OpM2Xtpsg%3A2%3A1624422931%3A-1%3A12377; fr=1OcGRL1n3BShLbN3B.AWW_gnMjqw0Aag8gQCzBdpyx7a0.Bg0rl0.9o.AAA.0.0.Bg0roT.AWWjxHM6Jnw; spin=r.1004018210_b.trunk_t.1624422933_s.1_v.2_"
    headers = {
        'Connection': 'keep-alive',
        'Cache-Control': 'max-age=0',
        'viewport-width': '1680',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-User': '?1',
        'Sec-Fetch-Dest': 'empty',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-ch-ua-mobile': '?0',
        'Cookie': f'{cookies_string}'
    }

    fb_datas = run(keyword.lower(), headers, account, max_result)
    print(str(len(fb_datas)))

    return fb_datas