# /
# @created 2023/05/16 - 15:12
# @author ductn
# /


def convert_detail_content(block_data, xpath_detail_content, xpath_image):

    list_content = []
    try:
        contents = block_data.contents

        for block_content in contents:

            if len(str(block_content)) < 4:
                continue

            type_data = check_type_data(block_data=block_content, xpath_image=xpath_image, xpath_content=xpath_detail_content)

            if type_data == 1:
                content = convert_content(block_data=block_content)
                if content is not None:
                    list_content.append(content)
            elif type_data == 2:
                list_content.append(convert_url(block_data=block_content))
    except:
        pass

    return list_content


def split_xpath(info_element, syntax_split):
    element_split = info_element.split(syntax_split)

    return element_split


def convert_class_name(block_data, attribute):
    name_class = ""
    try:
        list_class = block_data.attrs[attribute]
        if attribute == 'class':
            for attrs in list_class:

                if len(name_class) == 0:
                    name_class = attrs
                else:
                    name_class += " " + attrs
        else:
            name_class = list_class
    except:
        pass

    return name_class

def check_image(block_data, name_tag, element_images):

    """

    :param block_data:
    :param name_tag:
    :param element_images:
    :return: status (True: type image, False: type content)
    """

    status = False
    if block_data.find("img") is not None:
        return True
    if len(element_images) == 1 and name_tag == element_images[0]:
        status = True
    else:
        try:
            class_value = convert_class_name(block_data=block_data, attribute=element_images[1])
            if len(element_images) == 3 and element_images[2] == class_value:
                status = True
        except:
            pass

    return status


def check_content(name_tag, element_contents):

    """

    :param name_tag:
    :param element_contents:
    :return:
    """

    status = False
    if name_tag == element_contents[3]:
        status = True

    return status

def check_type_data(block_data, xpath_image, xpath_content):

    """
    :param xpath_content:
    :param block_data:
    :param xpath_image:
    :return: type_data (1: content, 2: image, 0: not map)
    """

    type_data= 1
    element_images = split_xpath(info_element=xpath_image, syntax_split="|")
    element_contents = split_xpath(info_element=xpath_content, syntax_split="|")
    name_tag= block_data.name

    status_image = check_image(name_tag=name_tag, element_images=element_images, block_data=block_data)
    status_content = check_content(name_tag=name_tag, element_contents=element_contents)

    if not status_content and not status_image:
        type_data= 0

    if status_image:
        type_data =2
    elif status_content:
        type_data=1

    return type_data


def convert_url(block_data):

    data = str(block_data)
    link_image = None

    """
    Get link image by 'src='
    """
    try:
        index_src = data.index("src=")

        data_split = data[index_src + 5:].split('"')

        link_image = list(filter(lambda x: x.startswith("http"), data_split))[0]
    except:
        pass

    if link_image is None:
        return None

    return {
        "content": link_image,
        "type": "img"
    }

def convert_content(block_data):

    content = None

    try:
        content = str(block_data.string)
    except:
        pass

    if (content is None or content == 'None') and len(block_data.text) > 5:
        content = block_data.text

    if content is None or content == 'None':
        return None

    return {
        "content": content,
        "type": "text"
    }



def convert_block_content(html_overview, xpath_content,xpath_image, detail_content_xpath):

    list_content = []
    element_detail_content = []
    element_split = split_xpath(info_element=xpath_content, syntax_split="|")
    if detail_content_xpath is not None and len(detail_content_xpath) > 1:
        element_detail_content = split_xpath(info_element=detail_content_xpath, syntax_split="|")

    blocks_data = html_overview.find(element_split[0], {element_split[1]: element_split[2]})
    contents = blocks_data.contents

    for block in contents:
        if len(str(block)) < 2:
            continue
        try:
            class_name = convert_class_name(block_data=block, attribute=element_detail_content[1])
            if len(element_detail_content) > 0 and element_detail_content[2] == class_name:
                list_content.extend(convert_detail_content(block_data=block, xpath_detail_content=detail_content_xpath, xpath_image=xpath_image))
                continue
        except:
            pass


        type_data = check_type_data(block_data=block, xpath_image=xpath_image, xpath_content=xpath_content)
        if type_data == 1:
            content = convert_content(block_data=block)
            if content is not None:
                list_content.append(content)
        elif type_data == 2:
            list_content.append(convert_url(block_data=block))

    return list_content