# /
# @created 2023/05/16 - 08:36
# @author ductn
# /

import re
from time import sleep
import unicodedata

from newspaper import Article
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
from lxml import html
from requests_html import HTMLSession
from urllib.parse import urlparse, unquote
import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry

from idhs.crawler.website_config.convert_content_news import convert_block_content


def get_domain_link(link_cate):

    element = urlparse(link_cate)

    return f"{element[0]}://{element[1]}"




def define_links(links, link_cate):

    list_link = []

    element_link = urlparse(link_cate)

    registeredDomain = element_link[1]

    for link in links:

        if str(link).startswith("http"):
            list_link.append(link)
        else:
            link = link if link.startswith("/") else f"/{link}"
            if registeredDomain == 'baohaiphong.com.vn':
                list_link.append(get_domain_link(link_cate) + "/baohp/vn/home" + link)
            else:
                list_link.append(get_domain_link(link_cate) + link)

    return list_link

def get_request(url):

    session = requests.Session()
    retry = Retry(connect=3, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)

    response = session.get(url, verify=False)
    response.encoding = response.apparent_encoding
    return response.text

def get_html(url):
    html = None
    try:
        article = Article(url=url)
        article.download()
        article.parse()
        sleep(1)

        html = article.html
    except:
        pass

    return html


def split_xpath(info_element, syntax_split):
    element_split = info_element.split(syntax_split)

    return element_split


def get_request_html(url):

    html_text = None
    try:
        session = HTMLSession()
        response = session.get(url=url)
        html_text = response.text
    except:
        pass

    return html_text


def get_list_link(link_cate, website_config):
    list_link = []
    link_cate = unquote(link_cate)
    response = get_html(url=link_cate)
    if response is None:
        response = get_request(url=link_cate)
    list_link_block = html.fromstring(response).xpath(website_config['link_config'])
    for href in list_link_block:
        try:
            if href.get("href") in list_link or 'javascript' in href.get("href"):
                continue
            list_link.append(href.get("href"))
        except:
            pass

    return list_link

def convert_author(xml_html, website_config):

    author = None

    try:
        author= xml_html.xpath(website_config['author_config'])[0].text
    except:
        pass

    return author


"""
convert time
"""

def convert_time(time_string, regex_time):
    time_string = unicodedata.normalize("NFKD", time_string).replace("\r", "").strip().replace("\n", "").replace("\\xa0", "")
    publish_date = None
    date_now = datetime.now()
    if unicodedata.normalize("NFKD", "giờ trước") in time_string:
        hours_value = None
        list_ele = time_string.split(' ')
        for element in list_ele:
            if hours_value is None:
                try:
                    hours_value = int(element)
                    break
                except:
                    pass
        try:
            publish_date = datetime.now() - timedelta(hours=hours_value)
            return publish_date
        except:
            pass
    if unicodedata.normalize("NFKD", "phút trước") in time_string:
        minutes_value = None
        list_element = time_string.split(' ')
        for element in list_element:
            if minutes_value is None:
                try:
                    minutes_value = int(element)
                    break
                except:
                    pass

        if minutes_value is not None:
            return datetime.now() - timedelta(minutes=minutes_value)

    temp = None
    if "/" in regex_time:
        temp = "/"
    elif "-" in regex_time:
        temp = "-"

    if unicodedata.normalize("NFKD", "tháng") in time_string.lower():


        if unicodedata.normalize("NFKD", "năm") in time_string.lower():
            time_string = time_string.lower().replace(unicodedata.normalize("NFKD", " năm "), temp)
        else:
            number = time_string.lower().index('t') + 9
            string_list = list(time_string)
            string_list[number] = temp
            time_string = "".join(string_list)

        time_string = time_string.lower().replace(unicodedata.normalize("NFKD", " tháng "), temp)

    try:
        time_string = time_string.replace(u'\xa0', u' ')
        time_string = time_string.replace(u'\t', u' ').replace("'", "")
        time_split = time_string.split(' ')

        date_define_value = None
        hour_define_value = None

        for t in time_split:
            try:
                if temp in t and len(t) > 3:
                    t = re.sub('[^0-9./-]', '', t)
                    date_define_value = t
                if ":" in t and hour_define_value is None and len(re.findall('[0-9]+', t)) > 0:
                    t = re.sub('[^0-9.:]', '', t)
                    hour_define_value = t
            except:
                pass
        if date_define_value is not None:
            if temp == "/":
                date_define_value = date_define_value.replace("-", "")
            date_define = date_define_value.replace(',', '')
            try:
                hour_define_value = hour_define_value.replace(',', '')
            except:
                pass

            date_define_value_split = date_define_value.split(temp)

            if len(date_define_value_split[0]) > 2:
                date_define_value = date_define_value_split[2] + temp + date_define_value_split[1] + temp + date_define_value_split[0]
            if hour_define_value is not None:
                if hour_define_value.count(":") == 1:
                    if len(hour_define_value.split(":")[1]) > 2:
                        hour_define_value = hour_define_value.split(":")[0] + ":" + hour_define_value.split(":")[1][0:2]
                    hours_define = (hour_define_value + ":00").replace(',', '')

                elif hour_define_value.count(":") == 3:
                    hours_define = hour_define_value[:hour_define_value.rindex(":")]
                else:
                    hours_define = hour_define_value
                date_time_str = date_define + " " + hours_define
                if len(date_define_value.split(temp)[-1]) == 2:
                    try:
                        date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%y %H:%M:%S')
                    except:
                        date_obj = datetime.fromisoformat(date_time_str)
                else:
                    try:
                        date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
                    except:
                        date_obj = datetime.fromisoformat(date_time_str)


            else:
                date_time_str = date_define + " " + str(date_now.hour) + ":" + str(date_now.minute) + ":" + str(
                    date_now.second)
                date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
            publish_date = date_obj
        elif hour_define_value is not None:
            date_time_str = str(date_now.day)+temp+str(date_now.month)+temp+str(date_now.year)+ " "
            if hour_define_value.count(":") == 1:
                hours_define = (hour_define_value + ":00").replace(',', '')

            elif hour_define_value.count(":") == 3:
                hours_define = hour_define_value[:hour_define_value.rindex(":")]
            else:
                hours_define = hour_define_value
            publish_date = datetime.strptime(date_time_str+hours_define, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
    except:
        pass
    return publish_date

def convert_publish_date(html_xml, time_xpath, time_regex):

    time_string = None
    """
    Get time by timestamp
    """
    try:
        time_string = str(html_xml.xpath(time_xpath))
        if int(time_string) > 4:
            return datetime.fromtimestamp(int(time_string))
    except:
        pass

    try:

        time_string = unicodedata.normalize("NFKD", time_string).replace("\r", "").strip().replace("\n", "").replace("\\xa0", "").replace("|", " ")

        if "Tháng" in time_string and "Tháng trước".lower() not in time_string.lower():
            time_string = time_string.replace("Tháng", "/")

        if "giờ" in time_string and "phút" in time_string and "tháng" in time_string:
            time_string = time_string.replace(" tháng ", "/").replace(" giờ", "").replace(" phút", "").replace(" , ",
                                                                                                               "/")
    except:
        pass

    publish_date = convert_time(time_string=time_string, regex_time=time_regex)

    return publish_date


def convert_description(description_config, parser_html):

    element_split = split_xpath(info_element=description_config, syntax_split='|')

    description = None

    try:
        description = parser_html.find(element_split[0], {element_split[1]: element_split[2]}).text
    except:
        pass

    return description


def get_detail_post(list_link, website_config):

    link_list = define_links(list_link, website_config['link_cate'])
    website_response = None
    for link in link_list:
        link = unquote(link)

        try:
            title, description, author, publish_date = None, None, None, None
            list_content = []
            response_detail = get_html(url=link)

            if response_detail is None:
                response_detail = get_request(link)
            parser_html = BeautifulSoup(response_detail, "lxml")
            xml_html = html.fromstring(response_detail)
            try:
                title = xml_html.xpath(website_config['title_config'])[0].text
            except:
                try:
                    title = str(xml_html.xpath(website_config['title_config'])[0])
                except:
                    pass
            try:
                description = convert_description(website_config['description_config'], parser_html)
            except:
                pass
            try:
                author = convert_author(xml_html=xml_html, website_config=website_config)
            except:
                pass
            try:
                list_content = convert_block_content(html_overview=parser_html, xpath_content=website_config['content_config'],
                                                 xpath_image=website_config['image_config'], detail_content_xpath=website_config['detail_content_config'])
            except:
                pass

            try:
                publish_date = convert_publish_date(html_xml=xml_html, time_xpath=website_config['time_config'], time_regex=website_config['time_regex'])
            except:
                pass

            website_response = {
                "listLink": list_link,
                "title": title,
                "description": description,
                "author": author,
                "contents": list_content,
                "linkPost": link
            }

            if publish_date is not None:
                website_response['publishDate'] = publish_date.strftime('%Y-%m-%dT%H:%M:%SZ')
            else:
                website_response['publishDate'] = None

            if len(list_content) == 0 and title is None and description is None and author is None:
                website_response = None

            if website_response is not None:
                break
        except:
            pass

    if website_response is None and len(list_link) > 0:
        website_response = {
            "listLink": list_link
        }

    return website_response


def executeData(website_config):

    link_cate = website_config['link_cate']
    list_link = get_list_link(link_cate=link_cate, website_config=website_config)
    if len(list_link) == 0:
        return {
                "listLink": [],
                "title": None,
                "description": None,
                "author": None,
                "contents": [],
                "publishDate": None,
                "linkPost": None
            }

    return get_detail_post(list_link=list_link, website_config=website_config)


# if __name__ == '__main__':
#
#
#     website_config = {
#         "title_config": "//h1",
#         "time_config": "//div[@id='post-date']//text()",
#         "content_config": "div|id|post-content|p",
#         "detail_content_config": "",
#         "description_config": "h4|class|mb-2",
#         "author_config": "//p[@style='text-align: right;']//strong",
#         "image_config": "p|style|text-align: center;",
#         "link_cate": "https://baocamau.vn/thoi-su/",
#         "time_regex": "%d/%m/%Y",
#         "link_config": "//div[@class='post-image']//a",
#     }
#
#     data = executeData(website_config)
#     print("1")


