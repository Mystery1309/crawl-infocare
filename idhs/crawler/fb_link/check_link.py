# /
# @created 2023/03/31 - 15:47
# @author ductn
# /
import requests

def executeLinkPost(link_post, cookies_string):
    type_link = None

    headers = {
        'Connection': 'keep-alive',
        'Cache-Control': 'max-age=0',
        'viewport-width': '1680',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-User': '?1',
        'Sec-Fetch-Dest': 'empty',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*https://www.facebook.com/groups/1450914848556053;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-ch-ua-mobile': '?0',
        'Cookie': f'{cookies_string}'
    }

    response_request = requests.get(link_post, headers=headers)
    html_view = response_request.text
    id_response = None
    if html_view.find('"group_id"') > 0:
        type_link = "group"
        id_response = html_view[html_view.find('group_id":"') + 11:].split('"')[0]
    elif html_view.find('"PAGE_TIMELINE"') > 0 or html_view.find('"associated_page_id"') > 0:
        type_link = "page"
        try:
            id_response = int(html_view[html_view.find("page_id=") + 8:].split('"')[0])
        except:
            id_response = int(html_view[html_view.find('"userID":') + 10:].split('"')[0])


    elif html_view.find('"profile_id"') > 0 or html_view.find('"profile_user":{"name"') > 0:
        type_link = "profile"
        id_response = html_view[html_view.find('container_id') + 15:].split('"')[0]

    if type_link is None and id_response is None:
        type_link = "group_private"
        id_response = html_view[html_view.find('groupID')+10:].split('"')[0]
    return type_link, id_response


if __name__ == '__main__':


    type_response = executeLinkPost(link_post="https://www.facebook.com/groups/ViecLamNganhCongNgheThucPham.HoaChat.MoiTruong/", cookies_string=None)

    print("1")
