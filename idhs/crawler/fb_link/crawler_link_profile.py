# /
# @created 2023/03/31 - 15:39
# @author ductn
# /

from time import sleep
from datetime import datetime

from idhs.common.handle.exception import *
from idhs.common.model.base_object import FullContent
from bs4 import BeautifulSoup
import re
import random
import json
import requests

from idhs.common.model.base_response import FacebookGroupCommentCare, BaseResponse


def get_interactive(feedback_target_with_context, name_count):
    count = 0
    try:
        count = feedback_target_with_context.get('ufi_renderer').get('feedback').get(
            "comet_ufi_summary_and_actions_renderer").get(
            "feedback").get(name_count).get("count")
    except:
        pass

    return count


def check_content(content, topics):
    responses = None
    for topic in topics:
        if topic.lower() in content.lower():
            if responses is None:
                responses = topic
            else:
                responses += ", " + topic

    return responses


def get_comment_count(feedback_target_with_context):
    count_comment = 0
    try:
        count_comment = feedback_target_with_context.get('ufi_renderer').get('feedback').get('total_comment_count')
        count_comment = int(count_comment)
    except Exception as e:
        try:
            count_comment = feedback_target_with_context['ufi_renderer']['feedback']['comet_ufi_summary_and_actions_renderer'][
                'feedback']['total_comment_count']
            count_comment = int(count_comment)
        except:
            pass

    return count_comment


def handle_interactions(pool_reaction):
    count = 0
    try:
        count = pool_reaction.get("reaction_count")
    except:
        pass

    return count


def convertContentPost(full_contents):
    contentData = None
    for full_content in full_contents:
        if contentData is None:
            contentData = full_content['content']
        else:
            contentData += " \n" + full_content['content']

    return contentData


def get_full_contents(message, node_comet_sections):
    full_contents = []
    message_shares = None
    try:
        try:
            message_shares = node_comet_sections.get('content').get('story').get('attached_story').get(
                'comet_sections').get('message').get('story').get('message').get("text")
        except:
            pass

        if message is not None:
            string = message.get('story').get('message').get("text")
            contents = string.split("\n")
            for content in contents:
                full_contents.append(FullContent(id=str(content).__hash__(), content=content, type="text").get_dict())

        if message_shares is not None:
            contents = message_shares.split("\n")
            for content in contents:
                if content == "":
                    continue
                full_contents.append(FullContent(id=str(content).__hash__(), content=content, type="text").get_dict())
    except Exception as e:

        print(e)
        print("Error full_content facebook group type post")

    if len(full_contents) == 0:
        try:
            message_content = node_comet_sections['content']['story']['message']['text']
            full_contents.append(FullContent(id=str(message_content).__hash__(), content=message_content, type="text").get_dict())
        except:
            pass

    return full_contents


def parser_data(data):
    save_data = None
    post_id = None
    try:
        node_comet_sections = data.get("data").get("node").get("comet_sections")
        feedback_target_with_context = node_comet_sections.get("feedback").get("story").get("feedback_context").get(
            "feedback_target_with_context")
        story_comet_sections = node_comet_sections.get("context_layout").get("story").get("comet_sections")
        message_content = node_comet_sections.get("content").get("story").get('comet_sections').get('message')

        # TODO: get content of post
        full_contents = get_full_contents(message_content, node_comet_sections)
        title = None
        if len(full_contents):
            title = re.split('[,.]', full_contents[0].get("content"))[0]

        # TODO: get reaction count
        interaction = {'LIKE': 0, 'LOVE': 0, 'SUPPORT': 0, 'HAHA': 0, 'WOW': 0, 'ANGER': 0, 'SORRY': 0,
                       "share_count": get_interactive(feedback_target_with_context, "share_count"),
                       "reaction_count": get_interactive(feedback_target_with_context, "reaction_count"),
                       "comment_count": get_comment_count(feedback_target_with_context)}

        # TODO: get post id
        post_id = data['data']['node']['post_id']
        url = f"https://www.facebook.com/{post_id}"

        # TODO: get info group
        author_name = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get("name")
        author_id = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get("id")
        author_type = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get('__typename')

        # TODO: get public time of post
        try:
            publish_date_int = int(story_comet_sections.get('metadata')[0].get("story").get("creation_time"))
        except:
            publish_date_int = int(story_comet_sections.get('metadata')[1].get("story").get("creation_time"))
        publish_date = datetime.fromtimestamp(publish_date_int)

        if convertContentPost(full_contents) is None or len(convertContentPost(full_contents)) == 0:
            return data

        # TODO: check content post with topics
        save_data = BaseResponse(dataType="Facebook User", content=convertContentPost(full_contents),
                                     publishDate=publish_date, title=title, postId=post_id, createdDate=datetime.now(),
                                     likeCount=interaction["reaction_count"], shareCount=interaction["share_count"],
                                     commentCount=interaction["comment_count"], link=url,
                                     nameSource=author_name, authorId=author_id, authorType=author_type).get_serializable_dict()

    except Exception as e:
        print(e)

    return save_data, post_id


def get_token(response):
    response_index = response.index('"token":')
    index = response_index + 8
    response_string = response[index: index + 100]
    print(response_string)
    # Get token
    tokens = response_string.split('"')
    token = tokens[1]

    return token


def get_list_doc_string(script_list):
    list_doc_script = []
    for script_e in script_list:
        try:
            href = script_e.attrs.get("src")
            if href is None:
                continue
            if "https://static.xx.fbcdn.net/rsrc.php/v3" in href:
                list_doc_script.append(href)
        except:
            continue
    value0, value1, value2, value3 = None, None, None, None
    for doc_script in list_doc_script:
        try:
            response_comment = requests.get(doc_script)
            sleep(0.3)
            if response_comment.text == "":
                continue
            response_doc = response_comment.text.split("\n")
            for string in response_doc:
                if string.startswith(
                        '__d("CometSinglePostContentQuery_facebookRelayOperation"') and value0 is None:
                    value0 = string[string.index("e.exports=") + 11:].split('"')[0]

                if string.startswith('__d("CometUFICommentsProviderForDisplayCommentsQuery_facebookRelayOperation"') and value1 is None:
                    value1 = string[string.index("e.exports=")+11:].split('"')[0]

                if string.startswith('__d("CometUFIReactionsDialogQuery_facebookRelayOperation"') and value2 is None:
                    value2 = string[string.index("e.exports=")+11:].split('"')[0]

                if string.startswith('__d("CometResharesDialogQuery_facebookRelayOperation"') and value3 is None:
                    value3 = string[string.index("e.exports=")+11:].split('"')[0]

            if value0 is not None and value1 is not None and value2 is not None and value3 is not None:
                break
        except:
            continue

    return value0, value1, value2, value3

def get_cursor(parser_html):
    script_tag = parser_html.find_all('script')
    story_id = None
    feedback_id = None
    for script_text in script_tag:
        try:
            text = str(script_text.contents[0])

            if 'XCometContextualProfileControllerRouteBuilder' in text and '"story":{"id":' in text and story_id is None:
                story_id = text[text.index('"story":{"id":')+15:].split('"')[0]

            if 'CometUFICommentListRendererForDisplayComments_renderer' in text and feedback_id is None:
                feedback_id = text[text.index('"feedback":{"id":')+18:].split('"')[0]
        except:
            pass

        if story_id is not None and feedback_id is not None:
            break

    return story_id, feedback_id, script_tag


def get_variables(story_id, feedback_id):

    variable_post = {
        "UFI2CommentsProvider_commentsKey":"CometSinglePostRoute",
        "displayCommentsContextEnableComment":None,
        "displayCommentsContextIsAdPreview":None,
        "displayCommentsContextIsAggregatedShare":None,
        "displayCommentsContextIsStorySet":None,
        "displayCommentsFeedbackContext":None,
        "feedbackSource":2,
        "feedLocation":"PERMALINK",
        "focusCommentID":None,
        "hasScenario":False,
        "privacySelectorRenderLocation":"COMET_STREAM",
        "renderLocation":"permalink",
        "scale":1,
        "storyID":f"{story_id}",
        "useDefaultActor":False,
        "__relay_internal__pv__GroupsCometDelayCheckBlockedUsersrelayprovider":True,
        "__relay_internal__pv__IsWorkUserrelayprovider":False,
        "__relay_internal__pv__IsMergQAPollsrelayprovider":False,
        "__relay_internal__pv__StoriesArmadilloReplyEnabledrelayprovider":False,
        "__relay_internal__pv__StoriesRingrelayprovider":False
    }


    variable_comment = {"UFI2CommentsProvider_commentsKey":"CometSinglePostRoute",
                 "__false":False,
                 "__true":True,
                 "after":None,
                 "before":None,
                 "displayCommentsContextEnableComment":None,
                 "displayCommentsContextIsAdPreview":None,
                 "displayCommentsContextIsAggregatedShare":None,
                 "displayCommentsContextIsStorySet":None,
                 "displayCommentsFeedbackContext":None,
                 "feedLocation":"PERMALINK",
                 "feedbackSource":2,
                 "first":None,
                 "focusCommentID":None,
                 "includeHighlightedComments":False,
                 "includeNestedComments":True,
                 "initialViewOption":None,
                 "isInitialFetch":False,
                 "isPaginating":False,
                 "last":None,
                 "scale":1,
                 "topLevelViewOption":"RANKED_UNFILTERED",
                 "useDefaultActor":False,
                 "viewOption":"RANKED_UNFILTERED",
                 "id":f"{feedback_id}"
                 }
    variable_comment_string = json.dumps(variable_comment)
    variable_post_string = json.dumps(variable_post)
    return variable_post_string, variable_comment_string


def get_data_response(token, variable_string, doc_id_post, headers):
    form_data = dict()
    form_data["fb_dtsg"] = token
    form_data["variables"] = variable_string
    form_data["doc_id"] = doc_id_post
    form_data["__comet_req"] = 1
    form_data["__a"] = 1

    response_api = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)
    sleep(random.randint(1,3))
    parser_html_api = BeautifulSoup(response_api.text, "html.parser")
    class_no_api = parser_html_api.find_all("html", {"class": "no_js"})


    if response_api.status_code == 200 and len(class_no_api) == 0 and 'status==="ERROR"' not in response_api.text:
        data_response = json.loads(response_api.text.split("\n")[0])
        return data_response
    return None

def get_path_data(data_res):
    lis_path = []
    for data in data_res:
        feedUnits = data.get("data").get("node").get("group_feed").get("edges")
        lis_path.extend(feedUnits)

    return lis_path


def check_error(response_request):
    parser_html = BeautifulSoup(response_request.text, "html.parser")
    if 'status==="ERROR"' in response_request.text or len(parser_html.find_all("html", {"class": "no_js"})) > 0:
        raise CookieError("Cookies error !")

    else:
        response = response_request.text

    return parser_html, response


def get_data_comment(token, variable_string, doc_id_post, headers):
    list_data_res = []
    form_data = dict()
    form_data["fb_dtsg"] = token
    form_data["variables"] = variable_string
    form_data["doc_id"] = doc_id_post
    form_data["__comet_req"] = 1
    form_data["__a"] = 1

    variable_dict = json.loads(variable_string)
    count = 0
    after = None
    after_count = None
    comment_count = 0

    while count >= 0:

        if after is not None:
            variable_dict["after"] = after
            variable_dict["first"] = after_count
            form_data["variables"] = json.dumps(variable_dict)

        response_api = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)
        sleep(random.randint(3,8))
        parser_html_api = BeautifulSoup(response_api.text, "html.parser")
        class_no_api = parser_html_api.find_all("html", {"class": "no_js"})

        if response_api.status_code == 200 and len(class_no_api) == 0:
            data_response = json.loads(response_api.text.split("\n")[0])

            if comment_count == 0:
                comment_count = data_response['data']['node']['display_comments']['count']
            page_info = data_response['data']['node']['display_comments']['page_info']
            after = page_info['end_cursor']
            has_next_page = page_info['has_next_page']
            after_count = data_response['data']['node']['display_comments']['after_count']

            list_data_res.extend(data_response['data']['node']['display_comments']['edges'])
            count += 1
            if not has_next_page:
                break
        else:
            break

    return list_data_res, comment_count


def convert_reply_comment(edges, post_id):
    list_reply_comment = []
    for edge in edges:
        content = ""
        node = edge['node']
        comment_id = node['legacy_token']

        author = node['author']
        author_type = author['__typename']
        author_id = author['id']
        author_name = author['name']
        author_gender = author['gender']
        author_url = author['url']

        feedback = node['feedback']
        reaction = feedback['reactors']['count']
        reply_count = feedback['sub_replies_count']

        publish_date = datetime.fromtimestamp(node['created_time'])

        attachments = node['attachments']
        if len(attachments) > 0:
            try:
                content = attachments[0]['style_type_renderer']['attachment']['media']['image']['uri']
            except:
                pass

        body = node['body']
        if body is not None:
            content += " \n " + body['text']
        data_comment = FacebookGroupCommentCare(commentId=comment_id, postId=post_id, contentComment=content,
                                        authorName=author_name,
                                        authorLink=author_url, reactionCount=reaction, replyCount=reply_count,
                                        publishDate=publish_date, downloadDate=datetime.now(),
                                        linkComment=f"https://www.facebook.com/{comment_id}",
                                        authorGender=author_gender, authorType=author_type,
                                        authorId=author_id).get_serializable_dict()
        list_reply_comment.append(data_comment)

    return list_reply_comment


def convert_comments(datas_block, post_id):
    list_data = []
    for data_block in datas_block:
        try:
            content = ""
            node = data_block['node']
            comment_id = node['legacy_token']

            author = node['author']
            author_type = author['__typename']
            author_id = author['id']
            author_name = author['name']
            author_gender = author['gender']
            author_url = author['url']

            publish_date = datetime.fromtimestamp(node['created_time'])

            feedback = node['feedback']
            reaction = feedback['reactors']['count']
            reply_count = feedback['comment_count']['total_count']
            attachments = node['attachments']

            if len(attachments) > 0:
                try:
                    content = attachments[0]['style_type_renderer']['attachment']['url']
                except:
                    content = attachments[0]['style_type_renderer']['attachment']['media']['image']['uri']

            body = node['body']
            if body is not None:
                content += "\n " + body['text']



            data = FacebookGroupCommentCare(commentId=comment_id, postId=post_id, contentComment=content,
                                            authorName=author_name,
                                            authorLink=author_url, reactionCount=reaction, replyCount=reply_count,
                                            publishDate=publish_date, downloadDate=datetime.now(),
                                            linkComment=f"https://www.facebook.com/{comment_id}",
                                            authorGender=author_gender, authorType=author_type,
                                            authorId=author_id).get_serializable_dict()
            list_data.append(data)

            if reply_count > 0:
                edges = feedback['display_comments']['edges']
                list_data.extend(convert_reply_comment(edges=edges, post_id=post_id))
        except:
            pass

    return list_data


def convert_list_profile_like(data_response, post_id):
    list_profile_like = []
    node = data_response['data']['node']
    edges = node['reactors']['edges']

    for edge in edges:
        node_edge = edge['node']
        profile_id = node_edge['id']
        profile_name = node_edge['name']
        profile_type = node_edge['__typename']
        profile_picture = node_edge['profile_picture']['uri']

        if profile_type.lower() == "user":
            list_profile_like.append({
                "profileId": profile_id,
                "profileName": profile_name,
                "profileType": profile_type,
                "profilePicture": profile_picture,
                "postId": post_id,
                "type": "like"
            })
    return list_profile_like


def convert_reaction(data_response):
    reaction_count = 0
    node = data_response['data']['node']

    summaries = node['top_reactions']['summary']

    for summary in summaries:
        reaction_count += summary['reaction_count']

    return reaction_count


def get_profile_like(feedback_id, doc_id_reaction, fb_token, headers, post_id):

    form_data = dict()
    form_data["fb_dtsg"] = fb_token
    form_data["doc_id"] = doc_id_reaction
    form_data["__comet_req"] = 1
    form_data["__a"] = 1

    variable_share = {"feedbackTargetID":f"{feedback_id}",
                      "scale":1}

    form_data["variables"] = json.dumps(variable_share)

    response_api = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)
    sleep(random.randint(3, 8))
    parser_html_api = BeautifulSoup(response_api.text, "html.parser")
    class_no_api = parser_html_api.find_all("html", {"class": "no_js"})

    if response_api.status_code == 200 and len(class_no_api) == 0:
        data_response = json.loads(response_api.text.split("\n")[0])

        reaction_count = convert_reaction(data_response)

        list_profile_like = convert_list_profile_like(data_response, post_id)
        return reaction_count, list_profile_like



def convert_list_profile_share(edges, post_id):
    list_profile_share = []
    for edge in edges:
        node = edge['node']

        actors = node['comet_sections']['context_layout']['story']['comet_sections']['actor_photo']['story']['actors'][0]
        profile_type = actors['__typename']
        profile_id = actors['id']
        profile_name = actors['name']
        profile_picture = actors['profile_picture']['uri']

        if profile_type.lower() == "user":
            list_profile_share.append({
                "profileId": profile_id,
                "profileName": profile_name,
                "profileType": profile_type,
                "profilePicture": profile_picture,
                "postId": post_id,
                "type": "share"
            })

    return list_profile_share



def get_profile_share(feedback_id, doc_id_share, fb_token, headers, post_id):

    form_data = dict()
    form_data["fb_dtsg"] = fb_token
    form_data["doc_id"] = doc_id_share
    form_data["__comet_req"] = 1
    form_data["__a"] = 1

    variable_share = {"UFI2CommentsProvider_commentsKey":"CometResharesDialogQuery",
                      "feedbackID":f"{feedback_id}",
                      "feedbackSource":1,
                      "feedLocation":"SHARE_OVERLAY",
                      "privacySelectorRenderLocation":"COMET_STREAM",
                      "renderLocation":"reshares_dialog",
                      "scale":1,
                      "__relay_internal__pv__GroupsCometDelayCheckBlockedUsersrelayprovider":True,
                      "__relay_internal__pv__IsWorkUserrelayprovider":False,
                      "__relay_internal__pv__IsMergQAPollsrelayprovider":False,
                      "__relay_internal__pv__StoriesArmadilloReplyEnabledrelayprovider":False
                      }

    form_data["variables"] = json.dumps(variable_share)

    response_api = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)

    sleep(random.randint(3, 8))
    parser_html_api = BeautifulSoup(response_api.text, "html.parser")
    class_no_api = parser_html_api.find_all("html", {"class": "no_js"})

    list_profile_share = []

    if response_api.status_code == 200 and len(class_no_api) == 0:
        data_response = json.loads(response_api.text.split("\n")[0])
        edges = data_response['data']['feedback']['reshares']['edges']
        if len(edges) > 0:
            list_profile_share = convert_list_profile_share(edges, post_id)

    return list_profile_share


def run(link_profile, headers):
    response_request = requests.get(link_profile, headers=headers)

    sleep(random.randint(1,4))
    parser_html, response = check_error(response_request)
    try:
        token = get_token(response)
        story_id, feedback_id, script_tag = get_cursor(parser_html)

        doc_id_post, doc_id_comment, doc_id_reaction, doc_id_share = get_list_doc_string(script_tag)

        variable_post, variable_comment = get_variables(story_id=story_id, feedback_id=feedback_id)

        data_block = get_data_response(token, variable_post, doc_id_post, headers)

        data, post_id = parser_data(data=data_block)

        if data is not None:

            datas_block, comment_count = get_data_comment(token, variable_comment, doc_id_comment, headers)

            list_data_comment = convert_comments(datas_block, post_id=post_id)

            reaction_count, list_profile_like = get_profile_like(feedback_id=feedback_id, doc_id_reaction=doc_id_reaction,
                                                                 fb_token=token, headers=headers, post_id=post_id)

            list_profile_share = get_profile_share(feedback_id, doc_id_share, token, headers, post_id)

            data_res = {
                "reactionCount": reaction_count,
                "commentCount": comment_count,
                "listDataComment": list_data_comment,
                "listProfileLike": list_profile_like,
                "listProfileShare": list_profile_share,
                "postId": post_id
            }
            return data_res, data


    except Exception as e:
        print(e)

    return None, None


def executeData(link_profile, cookies_string):
    headers = {
        'Connection': 'keep-alive',
        'Cache-Control': 'max-age=0',
        'viewport-width': '1680',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-User': '?1',
        'Sec-Fetch-Dest': 'empty',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*https://www.facebook.com/groups/1450914848556053;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-ch-ua-mobile': '?0',
        'Cookie': f'{cookies_string}'
    }

    data, data_post = run(link_profile=link_profile, headers=headers)


    return data, data_post
