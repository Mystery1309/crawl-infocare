# /
# @created 2023/02/23 - 10:00
# @author ductn
# /
from time import sleep
from datetime import datetime

from idhs.common.handle.exception import *
from idhs.common.model.base_object import FullContent
from bs4 import BeautifulSoup
import re
import json
import requests
from idhs.common.model.base_response import BaseResponse
import random


def get_interactive(feedback_target_with_context, name_count):
    count = 0
    try:
        count = feedback_target_with_context.get('ufi_renderer').get('feedback').get(
            "comet_ufi_summary_and_actions_renderer").get(
            "feedback").get(name_count).get("count")
    except:
        pass

    return count


def check_content(content, topics):
    responses = None
    for topic in topics:
        if topic.lower() in content.lower():
            if responses is None:
                responses = topic
            else:
                responses += ", " + topic

    return responses


def get_comment_count(feedback_target_with_context):
    count_comment = 0
    try:
        count_comment = feedback_target_with_context.get('ufi_renderer').get('feedback').get('total_comment_count')
    except Exception as e:
        print(e)

    return count_comment


def handle_interactions(pool_reaction):
    count = 0
    try:
        count = pool_reaction.get("reaction_count")
    except:
        pass

    return count


def convertContentPost(full_contents):
    contentData = None
    for full_content in full_contents:
        if contentData is None:
            contentData = full_content['content']
        else:
            contentData += " \n" + full_content['content']

    return contentData


def get_full_contents(message, node_comet_sections):
    full_contents = []
    message_shares = None
    try:
        try:
            message_shares = node_comet_sections.get('content').get('story').get('attached_story').get(
                'comet_sections').get('message').get('story').get('message').get("text")
        except:
            pass

        if message is not None:
            string = message.get('story').get('message').get("text")
            contents = string.split("\n")
            for content in contents:
                full_contents.append(FullContent(id=str(content).__hash__(), content=content, type="text").get_dict())

        if message_shares is not None:
            contents = message_shares.split("\n")
            for content in contents:
                if content == "":
                    continue
                full_contents.append(FullContent(id=str(content).__hash__(), content=content, type="text").get_dict())
    except Exception as e:

        print(e)
        print("Error full_content facebook group type post")

    if len(full_contents) == 0:
        try:
            message_content = node_comet_sections['content']['story']['message']['text']
            full_contents.append(FullContent(id=str(message_content).__hash__(), content=message_content, type="text").get_dict())
        except:
            pass

    return full_contents


def parser_data(data):
    save_data = None
    try:
        node_comet_sections = data.get("node").get("comet_sections")
        feedback_target_with_context = node_comet_sections.get("feedback").get("story").get("feedback_context").get(
            "feedback_target_with_context")
        story_comet_sections = node_comet_sections.get("context_layout").get("story").get("comet_sections")
        message_content = node_comet_sections.get("content").get("story").get('comet_sections').get('message')

        # TODO: get content of post
        full_contents = get_full_contents(message_content, node_comet_sections)
        title = None
        if len(full_contents):
            title = re.split('[,.]', full_contents[0].get("content"))[0]

        # TODO: get reaction count
        interaction = {'LIKE': 0, 'LOVE': 0, 'SUPPORT': 0, 'HAHA': 0, 'WOW': 0, 'ANGER': 0, 'SORRY': 0,
                       "share_count": get_interactive(feedback_target_with_context, "share_count"),
                       "reaction_count": get_interactive(feedback_target_with_context, "reaction_count"),
                       "comment_count": get_comment_count(feedback_target_with_context)}

        # TODO: get post id
        post_id = data['node']['post_id']
        url = f"https://www.facebook.com/{post_id}"

        # TODO: get info group
        author_name = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get("name")
        author_id = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get("id")
        author_type = story_comet_sections.get("actor_photo").get("story").get("actors")[0].get('__typename')

        # TODO: get public time of post
        try:
            publish_date_int = int(story_comet_sections.get('metadata')[0].get("story").get("creation_time"))
        except:
            publish_date_int = int(story_comet_sections.get('metadata')[1].get("story").get("creation_time"))
        publish_date = datetime.fromtimestamp(publish_date_int)

        save_data = BaseResponse(dataType="Facebook Group", content=convertContentPost(full_contents),
                                     publishDate=publish_date, title=title, postId=post_id, createdDate=datetime.now(),
                                     likeCount=interaction["reaction_count"], shareCount=interaction["share_count"],
                                     commentCount=interaction["comment_count"], link=url,
                                     nameSource=author_name, authorId=author_id, authorType=author_type).get_serializable_dict()

    except Exception as e:
        print(e)

    return save_data


def get_token(response):
    response_index = response.index('"token":')
    index = response_index + 8
    response_string = response[index: index + 100]
    print(response_string)
    # Get token
    tokens = response_string.split('"')
    token = tokens[1]

    return token


def get_list_doc_string(script_list):
    list_doc_script = []
    for script_e in script_list:
        try:
            href = script_e.attrs.get("src")
            if href is None:
                continue
            if "https://static.xx.fbcdn.net/rsrc.php/v3" in href:
                list_doc_script.append(href)
        except:
            continue
    value0 = None
    for doc_script in list_doc_script:
        try:
            response_comment = requests.get(doc_script)
            sleep(0.3)
            if response_comment.text == "":
                continue
            response_doc = response_comment.text.split("\n")
            for string in response_doc:
                if string.startswith(
                        '__d("GroupsCometFeedRegularStoriesPaginationQuery_facebookRelayOperation"') and value0 is None:
                    value0 = string[string.index("e.exports=") + 11:].split('"')[0]

            if value0 is not None:
                break
        except:
            continue

    return value0


def get_cursor(parser_html):
    script_tag = parser_html.find_all('script')
    script = ""
    for script_text in script_tag:
        try:
            text = str(script_text.contents[0])

            if 'GroupsCometColorExpandedWrapper_group$normalization' in text and 'end_cursor' in text and '"has_next_page":true' in text:
                script = text
        except:
            continue

    data_json = json.loads(script)["require"][0][3][0]['__bbox']['require']

    _data = dict()
    for data in data_json:
        try:
            if 'RelayPrefetchedStreamCache' in data[0] and "adp_CometGroupDiscussionRootSuccessQueryRelayPreloader" in \
                    data[3][0]:
                _data = data[3][1]
                break

        except:
            continue

    end_cursor = _data['__bbox']['result']['data']['page_info']['end_cursor']

    return end_cursor, script_tag


def get_variables(cursor, group_id):
    variables = {"UFI2CommentsProvider_commentsKey": "CometGroupDiscussionRootSuccessQuery",
                 "count": 3,
                 "cursor": f"{cursor}",
                 "displayCommentsContextEnableComment": None,
                 "displayCommentsContextIsAdPreview": None,
                 "displayCommentsContextIsAggregatedShare": None,
                 "displayCommentsContextIsStorySet": None,
                 "displayCommentsFeedbackContext": None,
                 "feedLocation": "GROUP",
                 "feedType": "DISCUSSION",
                 "feedbackSource": 0,
                 "focusCommentID": None,
                 "privacySelectorRenderLocation": "COMET_STREAM",
                 "renderLocation": "group",
                 "scale": 1,
                 "sortingSetting": None,
                 "stream_initial_count": 20,
                 "useDefaultActor": False,
                 "id": f"{group_id}"}
    variable_string = json.dumps(variables)
    return variable_string


def get_data_response(token, variable_string, doc_id_post, headers):
    list_data_res = []
    form_data = dict()
    form_data["fb_dtsg"] = token
    form_data["variables"] = variable_string
    form_data["doc_id"] = doc_id_post
    form_data["__comet_req"] = 1
    form_data["__a"] = 1

    variable_dict = json.loads(variable_string)
    count = 0
    cursor = None

    while count >= 0:

        if cursor is not None:
            variable_dict["cursor"] = cursor
            form_data["variables"] = json.dumps(variable_dict)

        response_api = requests.post("https://www.facebook.com/api/graphql/", headers=headers, data=form_data)
        sleep(random.randint(1,3))
        parser_html_api = BeautifulSoup(response_api.text, "html.parser")
        class_no_api = parser_html_api.find_all("html", {"class": "no_js"})


        if response_api.status_code == 200 and len(class_no_api) == 0 and 'status==="ERROR"' not in response_api.text:
            data_response = json.loads(response_api.text.split("\n")[0])
            cursor = json.loads(response_api.text.split("\n")[1]).get("data").get("page_info").get('end_cursor')

            has_next_page = json.loads(response_api.text.split("\n")[1]).get("data").get("page_info").get(
                "has_next_page")
            list_data_res.append(data_response)
            count += 1
            if not has_next_page or len(list_data_res) > 12:
                break
        elif len(class_no_api) > 0 and 'status==="ERROR"' in response_api.text:
            break

    return list_data_res


def get_path_data(data_res):
    lis_path = []
    for data in data_res:
        feedUnits = data.get("data").get("node").get("group_feed").get("edges")
        lis_path.extend(feedUnits)

    return lis_path


def crawl_data(lis_path):
    list_data = []
    for data in lis_path:
        data_post = parser_data(data)
        if data_post is not None:
            list_data.append(data_post)

    return list_data


def check_error(response_request):
    parser_html = BeautifulSoup(response_request.text, "html.parser")
    if 'status==="ERROR"' in response_request.text or len(parser_html.find_all("html", {"class": "no_js"})) > 0:
        print("Cookies error !")
        raise CookieError("Cookies error !")

    else:
        response = response_request.text

    return parser_html, response


def get_group_id(response_request):

     return response_request.text[response_request.text.index("group_id")+11:].split('"')[0]


def run(link_group, headers, max_result):
    response_request = requests.get(link_group, headers=headers)
    sleep(random.randint(1,4))
    parser_html, response = check_error(response_request)
    group_id = get_group_id(response_request)
    try:
        token = get_token(response)
        cursor, script_tag = get_cursor(parser_html)

        doc_id_post = get_list_doc_string(script_tag)

        variable_string = get_variables(cursor, group_id)

        data_res = get_data_response(token, variable_string, doc_id_post, headers)

        lis_post_path = get_path_data(data_res)

        list_data = crawl_data(lis_path=lis_post_path)
        return list_data
    except Exception as e:
        print(e)


def executeData(link_group, cookies_string, max_result):
    headers = {
        'Connection': 'keep-alive',
        'Cache-Control': 'max-age=0',
        'viewport-width': '1680',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-User': '?1',
        'Sec-Fetch-Dest': 'empty',
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'sec-ch-ua-mobile': '?0',
        'Cookie': f'{cookies_string}'
    }

    list_data = run(link_group=link_group, headers=headers, max_result=max_result)


    return list_data



if __name__ == '__main__':

    datas = executeData(cookies_string="sb=xclIY3HWejSMVUU38R-DIeQw; m_pixel_ratio=1; x-referer=eyJyIjoiL3NlYXJjaC9wb3N0cy8%2FcT10aGUlMjBuYXR1cmUlMjBoYWlyJTIwc2Fsb24mc291cmNlPWZpbHRlciZpc1RyZW5kaW5nPTAmdHNpZD0wLjA2NTUzMDQ0ODMwNzcyMzAzIiwiaCI6Ii9zZWFyY2gvcG9zdHMvP3E9dGhlJTIwbmF0dXJlJTIwaGFpciUyMHNhbG9uJnNvdXJjZT1maWx0ZXImaXNUcmVuZGluZz0wJnRzaWQ9MC4wNjU1MzA0NDgzMDc3MjMwMyIsInMiOiJtIn0%3D; datr=w90TZFx_cLyummeSJRNiE3Nn; locale=vi_VN; c_user=100006140507104; wd=1920x975; xs=9%3AX5emJrzSE1h40g%3A2%3A1679451029%3A-1%3A6262%3A%3AAcXBmTB40j3HM7kYEckIq5hjw-KgdmGRTE8yhw2PhQ; fr=0OTRPe63dWsALrxMs.AWXmM0VlsdGR_zxC4xJ-krxjoYE.BkHRsX.qp.AAA.0.0.BkHRsX.AWVBtInuNjw; presence=C%7B%22t3%22%3A%5B%5D%2C%22utc3%22%3A1679629082888%2C%22v%22%3A1%7D",
                           link_group="https://www.facebook.com/groups/772719770198001/",max_result=30)
    print("1")


