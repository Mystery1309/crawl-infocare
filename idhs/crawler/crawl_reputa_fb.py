# /
# @created 2023/02/14 - 09:38
# @author ductn
# /

# /
# @created 2023/02/14 - 09:21
# @author ductn
# /

# /
# @created 2023/02/07 - 16:43
# @author ductn
# /

import json
import re
from datetime import datetime

import requests

from idhs.common.model.base_object import *
from idhs.common.model.base_response import BaseResponse

def convert_content(content):
    p =re.compile(r'(<!--.*?-->|<[^>]*>)')
    content = p.sub('', content)
    full_content = []
    text_split = content.split("\n")
    for c in text_split:
        full_content.append(FullContent(id=str(c).__hash__(), content=c, type="text").get_dict())

    return full_content


def run(BODY_FB, headers, account_reputa, max_result):
    list_crawl = []
    headers["Authorization"] = f"VTCCSSO {account_reputa.get('token')}"
    for idx in range(100):
        BODY_FB['page'] = idx
        response_fb = requests.post(url="https://apidn.reputa.vn/console/news/preview-search", json=BODY_FB, headers=headers)
        if response_fb.status_code != 200:

            return list_crawl
        response_data_fb = json.loads(response_fb.text)
        data_fb = response_data_fb.get('data')
        hits_fb = data_fb.get("hits")
        list_crawl.extend(convert_data_fb(hits_fb, headers, account_reputa))

        if len(list_crawl) >= max_result:
            break

    return list_crawl


def get_content(response) :

    full_contents = convert_content(json.loads(response).get('data'))

    return full_contents

def convert_data_fb(hits_fb, headers, account_reputa):
    list_data = []
    for data in hits_fb :

        url = data.get("url")

        list_url_split = url.split("/")

        post_id = list_url_split[-1] if len(list_url_split[-1]) > 0 else list_url_split[-2]

        reaction_count = data.get("like_count")
        share_count = data.get('share_count')
        comment_count = data.get('comment_count')
        age = None
        try:
            year_birth = data.get("author_year_of_birth")
            if int(year_birth) > 0 :
                age = str(datetime.year - int(year_birth))
        except:
            pass

        author_gender = data.get("author_gender")
        if author_gender == 1 :
            post_author_gender = GenderType.Male
        elif author_gender == 2 :
            post_author_gender = GenderType.FeMale
        else:
            post_author_gender = GenderType.Undefined
        from_object = ProfileFacebook(fid=data.get('author_id'), name=data.get('author_display_name')).get_dict()
        publish_date = datetime.strptime(data.get('published_time'), '%Y/%m/%d %H:%M:%S')

        source_type =None

        content_dict = {
            "ids": [data.get('id')],
            "topic": account_reputa.get("topic"),
            "keywords":[],
            "sub_keywords": [],
            "index_name": [data.get('index_name')]
        }
        contentPost = data['content']

        title = re.split('[,.]', contentPost)[0]
        if len(title) < 4:
            title = contentPost

        if data.get('article_type') == 'fb_page_post':

            source_type = "Facebook Fanpage"
        if data.get('article_type') == 'fb_group_post':
            source_type = "Facebook Group"
        if data.get('article_type') == 'fb_user_post':
            source_type = "Facebook User"

        save_data = BaseResponse(dataType=source_type, content=contentPost,
                                     publishDate=publish_date, title=title, postId=post_id, createdDate=datetime.now(),
                                     likeCount=reaction_count, shareCount=share_count,
                                     commentCount=comment_count, link=url,
                                     nameSource=from_object['name'], authorId=from_object['fid'], authorType="Page").get_serializable_dict()

        list_data.append(save_data)


    return list_data


def executeData(main_keywords, topic, token, user, brand_type, max_result):
    BODY_FB = {
        "size": 1000,
        "length": 1000,
        "page":0,
        "included_keywords": [
            {
                "main_keywords": [f"{main_keywords}"
                                  ],
                "sub_keywords": []
            }
        ],
        "topic_source": {
            "facebook_user_post": 1,
            "facebook_group_post": 1,
            "facebook_page_post": 1
        }
    }

    headers = {'Connection': 'keep-alive', 'Cache-Control': 'max-age=0', 'viewport-width': '1680',
               'Upgrade-Insecure-Requests': '1',
               'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36',
               'Sec-Fetch-Site': 'none', 'Sec-Fetch-Mode': 'navigate', 'Sec-Fetch-User': '?1',
               'Sec-Fetch-Dest': 'document',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9'}

    account_reputa = {
        "user": user,
        'topic': topic,
        'token': token
    }

    datas = run(BODY_FB=BODY_FB, headers=headers, account_reputa=account_reputa, keySearch=main_keywords, brand_type=brand_type, max_result=max_result)
    print(len(datas))
    return datas


