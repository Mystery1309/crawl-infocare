# Created by truonglx.93 at 8/28/20
from idhs.common.model.base_object import BeanContentData, BeanType, SpecialFields, CentralType, GenderType


class ForumsSpecialFields(SpecialFields):
    def __init__(self, comment_count, reaction_count, like, dislike, id, from_obj, to_obj):
        self.comment_count = comment_count
        self.reaction_count = reaction_count
        self.like = like
        self.dislike = dislike
        self.id = id
        self.from_obj = from_obj
        self.to_obj = to_obj

    def get_serializable_dict(self):
        return self.get_dict()

    def get_dict(self):
        return {
            "comment_count": self.comment_count,
            "reaction_count": self.reaction_count,
            "like_count": self.like,
            "dislike_count": self.dislike,
            "id": self.id,
            "from": self.from_obj,
            "to": self.to_obj
        }


class ForumsBeanContent(BeanContentData):

    def __init__(self, bean_type: BeanType, download_date, full_contents: list, publish_date, title,
                 description, url,
                 attachment_urls: list, domain, special_fields: SpecialFields, origin, source_id, source_name,
                 hastags:list):
        super().__init__(bean_type=bean_type, download_date=download_date,
                         full_contents=full_contents,
                         publish_date=publish_date, title=title, description=description, url=url,
                         attachment_urls=attachment_urls, domain=domain, special_fields=special_fields, origin=origin,
                         source_id=source_id, source_name=source_name, post_author_central=CentralType.Other,
                         post_author_region="Việt Nam", post_author_gender=GenderType.Undefined,
                         post_author_age=None, post_author_location=None, hastags=hastags)

    def get_serializable_dict(self):
        return super().get_serializable_dict()
