from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from idhs.common.model.base_object import FullContent, AttachmentUrl, BeanType
from idhs.common.model.news_object import NewsSpecialFields, NewsBeanContent
from idhs.module_forum.model.forums_object import ForumsSpecialFields, ForumsBeanContent

from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
import requests
from datetime import datetime, timedelta
import undetected_chromedriver as uc
from lxml import html


def convert_url(driver, message):
    url_list = []
    for i in range(2):
        a_list = driver.find_elements(By.XPATH, message["linksPostXpath"])
        url_list += [link.get_attribute("href") for link in a_list]
        driver.find_elements(By.XPATH, message["nextPageXpath"])[0].click
    return url_list

def convert_top_content(content_html, message):
    title = html.fromstring(content_html).xpath(message["titleXpath"])[0].text
    if title == None:
        title = ' '.join(html.fromstring(content_html).xpath(message["titleXpath"]+"//text()"))
    publish_date = html.fromstring(content_html).xpath("//time[@class='u-dt']")[0].attrib['datetime'][:-5]
    author = html.fromstring(content_html).xpath(message["authorXpath"])[0].text
    return title, publish_date, author

def convert_content(content_html, message):
    full_contents = []
    attachment_urls = []
    content_split = message["contentXpath"].split("|")
    content_body = BeautifulSoup(content_html, "lxml").find(content_split[0], {content_split[1]: content_split[2]})
    content_text_list = content_body.text.split("\n")
    for text in content_text_list:
        if text != "" and text != "Click to expand...":
            full_contents.append(FullContent(content=text, id=str(text).__hash__(), type="text").get_dict())
    img_list = content_body.select("img")
    for img in img_list:
        attachment_urls.append(FullContent(content=img.attrs["src"], id=str(img.attrs["src"]).__hash__(), type="img").get_dict())
    return full_contents, attachment_urls

def convert_comment(url, content_html, message):
    comment_content_split = message["commentContentXpath"].split("|")
    comment_element_list = BeautifulSoup(content_html, "lxml").findAll(comment_content_split[0], {comment_content_split[1] : comment_content_split[2]})
    comment_list = []
    i = 1
    comment_element_list.pop(0)
    for comment in comment_element_list:
        full_contents = []
        attachment_urls = []
        comment_author = html.fromstring(content_html).xpath(message["commentAuthorXpath"])[i].text
        comment_time = html.fromstring(content_html).xpath(message["commentTimeXpath"])[i].attrib["datetime"][:-5]
        comment_url = message["linkDomain"] + html.fromstring(content_html).xpath(message["commentUrlXpath"])[i].attrib["href"]
        for text in comment.text.split("\n"):
            if text != "" and text != "Click to expand...":
                full_contents.append(FullContent(content=text, id=str(text).__hash__(), type="text").get_dict())
        for img in comment.select("img"):
            attachment_urls.append(FullContent(content=img.attrs["src"], id=str(img.attrs["src"]).__hash__(), type="img").get_dict())
        comment_special_fields = ForumsSpecialFields(comment_count=0, reaction_count=0,
                                                   like=0, dislike=0,
                                                   id=comment_url.split("/")[-1], from_obj=None,
                                                   to_obj=None)
        comment_obj = {
            "author": comment_author,
            "time": comment_time,
            "content": full_contents,
            "attachments": attachment_urls,
            "special_fields": comment_special_fields,
            "url": comment_url,
        }
        comment_list.append(comment_obj)
        i += 1
        if i == len(comment_element_list):
            break
    commentCount = len(comment_element_list) - 1
    reactionCount = 0
    likeCount = 0
    from_object = None
    to_object = None
    forum_special_fields = ForumsSpecialFields(comment_count=commentCount, reaction_count=reactionCount,
                                               like=likeCount, dislike=0,
                                               id=url.split("/")[-1] if len(url.split("/")[-1]) > 0 else url.split("/")[-2], from_obj=from_object,
                                               to_obj=to_object)
    return comment_list, forum_special_fields

def run(message):
    # ser = Service("etc/chromedriver")
    # op = webdriver.ChromeOptions()
    # op.add_argument('headless')
    # driver = webdriver.Chrome(service=ser, options=op)
    driver = uc.Chrome()
    driver.get(message["linkCate"])
    url_list = convert_url(driver, message)
    list_data = []
    for url in url_list:
        try:
            driver.get(url)
            content_html = driver.page_source
            title, publish_date, author = convert_top_content(content_html, message)
            full_contents, attachment_urls = convert_content(content_html, message)
            comment_list, forum_special_fields = convert_comment(url, content_html, message)
            data_dict = ForumsBeanContent(bean_type=BeanType.bean_content_forum_post, download_date=datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
                              full_contents=full_contents, publish_date=publish_date, title=title,
                              description=None,
                              url=url, attachment_urls=attachment_urls, domain=message["linkDomain"],
                              special_fields=forum_special_fields,
                              origin=0, source_id=None, source_name=author,
                              hastags=[]).get_dict()
            list_data.append(data_dict)

            for comment in comment_list:
                data = ForumsBeanContent(bean_type=BeanType.bean_content_forum_comment,
                                         download_date=datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
                                         full_contents=comment["content"], publish_date=comment["time"], title=None,
                                         description=None,
                                         url=comment["url"], attachment_urls=comment["attachments"], domain=message["linkDomain"],
                                         special_fields=comment["special_fields"],
                                         origin=0, source_id=None, source_name=comment["author"],
                                         hastags=[]).get_dict()
                list_data.append(data)
        except:
            pass
    driver.close()
    return list_data


if __name__ == "__main__":
    message = {
        "linkCate": "https://voz.vn/whats-new/posts/",
        "linksPostXpath": "//div[@class='structItem-title']//a",
        "timeXpath": "//time[@class='u-dt']",
        "authorXpath": "//a[@class='username  u-concealed']",
        "titleXpath": "//h1[@class='p-title-value']",
        "contentXpath": "div|class|message-content js-messageContent",
        "nameDomain": "voz.vn",
        "linkDomain": "https://voz.vn",
        "imageClass": "bImage",
        "timeRegex": "",
        "nextPageXpath": "//a[@class='pageNav-jump pageNav-jump--next']",
        "commentListXpath": "//div[@class='message-inner']",
        "commentAuthorXpath": "//h4[@class='message-name']//a",
        "commentTimeXpath": "//li[@class='u-concealed']//a/time",
        "commentUrlXpath": "//li[@class='u-concealed']//a",
        "commentContentXpath": "article|class|message-body js-selectToQuote"
    }

    datas = run(message)
    print(1)
