from flask import Flask, jsonify

import werkzeug

werkzeug.cached_property = werkzeug.utils.cached_property
from flask_restplus import Api as rapi
from flask_restplus import Api, Resource, fields
import idhs.crawler.crawl_reputa_fb as crawler_reputa
import idhs.crawler.crawl_reputa_exist as crawler_reputa_news
import idhs.crawler.crawl_ytb_search as crawler_ytb
import idhs.crawler.crawl_fb_group_public as crawler_group
import idhs.crawler.crawl_fb_page as crawler_page
import idhs.crawler.crawl_fb_search as crawler_fb
from idhs.crawler.fb_link import check_link, crawler_link_page, crawler_link_group, crawler_link_profile
from idhs.common.kafka.producerKafka import MessageProducer
from idhs.crawler.website_config import crawler_website as crawler_website
from idhs.crawler.hashtag.hashtag_instagram import resolved_data
from idhs.crawler.hashtag.hashtag_fb import resolved_link
from idhs.crawler.hashtag.hashtag_ytb import resolved_link as check_hashtag_ytb
from idhs.crawler.stock_price.get_price import execute

flask_app = Flask(__name__)
app = Api(app=flask_app,
          title="Swagger Crawler",
          description="Crawler Data By body request")
namespace = app.namespace("crawler", description='Crawler APIs')

model = app.model('ModelCrawler', {'token': fields.String(required=True, description="Cookies của người dùng đã login"),
                                   "keyword": fields.String(required=True,
                                                            description="Từ khóa người dùng muốn tìm kiếm"),
                                   "topic": fields.String(required=False,
                                                          description="Chủ đề của tài khỏan đã tạo với loại dữ liệu Reputa"),
                                   "username": fields.String(required=False,
                                                             description="Gmail tài khoản đang sử dụng để lấy Cookies"),
                                   "typeSearch": fields.String(required=True, description="Loại dữ liệu bạn muốn crawl",
                                                               enum=["reputa", "fb_search", "ytb"]),
                                   "maxResult": fields.String(required=True,
                                                              description="Số lượng kết quả bạn muốn search"),
                                   "timeNumber": fields.String(required=True,
                                                               description="Dữ liệu sẽ nằm trong khoảng ngày"),
                                   "link": fields.String(required=True, description="URL bạn muốn lấy data")
                                   })

parser = app.parser()
parser.add_argument('keyword', type=str)
parser.add_argument('topic', type=int)
parser.add_argument('token', type=str)
parser.add_argument('username', type=str)
parser.add_argument('typeSearch', type=str)
parser.add_argument('maxResult', type=int)
parser.add_argument('timeNumber', type=int)
parser.add_argument('link', type=str)

namespace_website = app.namespace("website", description='View data website by config')
model_website = app.model('ConfigCrawler',
                          {'titleConfig': fields.String(required=True, description="Xpath get title data"),
                           "timeConfig": fields.String(required=True,
                                                       description="Xpath time get publish date of post"),
                           "contentConfig": fields.String(required=False,
                                                          description="Config lấy content theo định dạng: tag|attr|value|tagContent"),
                           "detailContentConfig": fields.String(required=False,
                                                                description="Config lấy content trong thẻ con của contentConfig"),
                           "descriptionConfig": fields.String(required=True, description="Lấy sapo của bài báo"),
                           "authorConfig": fields.String(required=True,
                                                         description="Config lấy author bài viết được lấy theo định dạng : tag|attr|value"),
                           "imageConfig": fields.String(required=True,
                                                        description="Config để lấy ảnh theo định dang: tag|class_value"),
                           "linkCate": fields.String(required=True, description="Link bạn muốn xử lý"),
                           "timeRegex": fields.String(required=True,
                                                      description="Định dạng ngày tháng của trang báo : %d/%m/%Y"),
                           "linkConfig": fields.String(required=True,
                                                       description="Config lấy list link post của category")
                           })

parser_website = app.parser()
parser_website.add_argument('titleConfig', type=str)
parser_website.add_argument('timeConfig', type=str)
parser_website.add_argument('contentConfig', type=str)
parser_website.add_argument('detailContentConfig', type=str)
parser_website.add_argument('descriptionConfig', type=str)
parser_website.add_argument('authorConfig', type=str)
parser_website.add_argument('imageConfig', type=str)
parser_website.add_argument('linkCate', type=str)
parser_website.add_argument('timeRegex', type=str)
parser_website.add_argument('linkConfig', type=str)

namespace_fb_type = app.namespace("facebookType", description='View link facebook')
model_fb_type = app.model('FbType', {
    'link': fields.String(required=True, description="Link Fb check")
})

parser_fb_type = app.parser()
parser_fb_type.add_argument('link', type=str)

namespace_hashtag = app.namespace("hashtagType", description="Check exist hashtag type")
model_hashtag = app.model('HashtagType', {
    'hashtagName': fields.String(required=True, description="Hashtag name check")
})
parser_hashtag_type = app.parser()
parser_hashtag_type.add_argument('hashtagName', type=str)

model_stock_data = app.model('StockMonitoring', {
    'stockName': fields.String(required=True, description="Stock name"),
    'dayNumber': fields.Integer(required=True, description="Stock name")
})
parser_stock_monitoring = app.parser()
parser_stock_monitoring.add_argument('stockName', type=str)
parser_stock_monitoring.add_argument('dayNumber', type=int)


@namespace.route("/")
class MainClass(Resource):

    @app.doc(parser=parser)
    @app.expect(model)
    def post(self):
        list_data = []
        data = parser.parse_args()
        request_client = {
            "keyword": data['keyword'],
            "topic": data['topic'],
            "token": data['token'],
            "username": data['username'],
            "typeSearch": data['typeSearch'],
            "maxResult": data['maxResult'],
            "timeNumber": data['timeNumber'],
            "link": data['link'],
        }

        if request_client.get("typeSearch") == "reputa":
            list_data = crawler_reputa.executeData(main_keywords=request_client['keyword'],
                                                   topic=request_client['topic'],
                                                   token=request_client['token'], user=request_client['username'],
                                                   brand_type=request_client['brandType'],
                                                   max_result=request_client['maxResult'])

        if request_client.get("typeSearch") == "reputa_news":
            list_data = crawler_reputa_news.executeData(main_keywords=request_client['keyword'],
                                                        max_result=request_client['maxResult'])

        if request_client.get("typeSearch") == "ytb":
            list_data = crawler_ytb.executeRequest(token=request_client['token'], keyword=request_client['keyword'],
                                                   brand_type=request_client['brandType'],
                                                   time_num=request_client['timeNumber'],
                                                   max_result=request_client['maxResult'])

        if request_client.get("typeSearch") == "fb_search":
            list_data = crawler_fb.executeRequest(cookies_string=request_client['token'],
                                                  keyword=request_client['keyword'],
                                                  max_result=request_client['maxResult'])

        if request_client.get("typeSearch") == "fb_page":
            list_data = crawler_page.executeData(cookies_string=request_client['token'],
                                                 link_page=request_client['link'], topics=request_client['keyword'],
                                                 brand_type=request_client['brandType'],
                                                 max_result=request_client['maxResult'])

        if request_client.get("typeSearch") == "fb_group_public":
            list_data = crawler_group.executeData(cookies_string=request_client['token'],
                                                  link_group=request_client['link'],
                                                  max_result=request_client['maxResult'])

        if request_client.get("typeSearch") == "fb_link":
            links = request_client['link'].split(" ")
            for link_post in links:
                if len(link_post) == 0:
                    continue
                data = None
                dataPost = None
                if 'groups' in link_post:
                    typeData = "group"
                else:
                    typeData, id_ = check_link.executeLinkPost(link_post=link_post,
                                                               cookies_string=request_client['token'])
                if typeData == "group":
                    data, dataPost = crawler_link_group.executeData(link_group=link_post,
                                                                    cookies_string=request_client['token'])
                elif typeData == "page":
                    data, dataPost = crawler_link_page.executeData(link_page=link_post,
                                                                   cookies_string=request_client['token'])
                elif typeData == "profile":
                    data, dataPost = crawler_link_profile.executeData(link_profile=link_post,
                                                                      cookies_string=request_client['token'])

                if data is not None and dataPost is not None:
                    try:
                        list_data.append(dataPost)
                    except:
                        pass

                    try:
                        MessageProducer().send_msg_fb_comment(data)
                    except:
                        pass

        MessageProducer().send_msg_fb_group_post(list_data)
        return jsonify(result=list_data)


@namespace.route("/website")
class Website(Resource):

    @app.doc(parser=parser_website)
    @app.expect(model_website)
    def post(self):
        data = parser_website.parse_args()
        data_website = dict()

        request_client = {
            "title_config": data['titleConfig'],
            "time_config": data['timeConfig'],
            "content_config": data['contentConfig'],
            "detail_content_config": data['detailContentConfig'],
            "description_config": data['descriptionConfig'],
            "author_config": data['authorConfig'],
            "image_config": data['imageConfig'],
            "link_cate": data['linkCate'],
            "time_regex": data['timeRegex'],
            "link_config": data['linkConfig'],
        }

        data_website = crawler_website.executeData(website_config=request_client)

        return jsonify(data_website)


@namespace.route("/check-type")
class FacebookType(Resource):

    @app.doc(parser=parser_fb_type)
    @app.expect(model_fb_type)
    def post(self):
        data = parser_fb_type.parse_args()

        request_client = {
            "link": data['link']
        }

        typeLink, idLink = check_link.executeLinkPost(link_post=request_client['link'], cookies_string=None)

        data_website = {
            "type": typeLink,
            "id": idLink
        }
        return jsonify(data_website)


@namespace.route("/check-hashtag")
class HashtagType(Resource):
    @app.doc(parser=parser_hashtag_type)
    @app.expect(model_hashtag)
    def post(self):
        data = parser_hashtag_type.parse_args()

        request_client = {
            "hashtagName": data['hashtagName']
        }

        responseCheckFB = resolved_link(hashtag_name=request_client['hashtagName'])
        responseCheckInstagram = resolved_data(hashtag_name=request_client['hashtagName'])
        responseCheckYtb = check_hashtag_ytb(hashtag_name=request_client['hashtagName'])

        data_website = {
            "statusFb": responseCheckFB,
            "statusInstagram": responseCheckInstagram,
            "statusYtb": responseCheckYtb,
        }
        return jsonify(data_website)


@namespace.route("/get-price")
class StockMonitoring(Resource):

    @app.doc(parser_stock_monitoring)
    @app.expect(model_stock_data)
    def post(self):

        data = parser_stock_monitoring.parse_args()

        list_stock_data = execute(stockName=data['stockName'], dayNumber=data['dayNumber'])

        response = {
            "datas": list_stock_data,
            "stockName": data['stockName']
        }

        return jsonify(response)


@property
def _specs_url(self):
    """Fixes issue where swagger-ui makes a call to swagger.json over HTTP.
       This can ONLY be used on servers that actually use HTTPS.  On servers that use HTTP,
       this code should not be used at all.
    """
    import flask
    return flask.url_for(self.endpoint('specs'), _external=True, _scheme='https')
    # return flask.url_for(self.endpoint('specs'), _external=True, _scheme='http')


if __name__ == '__main__':
    rapi.specs_url = _specs_url
    flask_app.run(debug=True, host="0.0.0.0")
