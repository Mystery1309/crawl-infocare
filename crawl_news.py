import re
from datetime import datetime, timedelta

import requests
from lxml import html
from bs4 import BeautifulSoup
from idhs.common.model.base_object import FullContent, AttachmentUrl, BeanType
from idhs.common.model.news_object import NewsSpecialFields, NewsBeanContent



import unicodedata
from lxml import html
from bs4 import BeautifulSoup
from idhs.common.model.base_object import FullContent, AttachmentUrl, BeanType
from idhs.common.model.news_object import NewsSpecialFields, NewsBeanContent


# from idhs.common.config.config_load import config
WORKER_FACEBOOK = 'worker-facebook'

# service_name = config.get(WORKER_FACEBOOK).get("service_name")

api_exception_call = "https://api.telegram.org/bot5584708586:AAF7z8XNsvL51-BMqPNc86C6mlcM2dJ2g40/sendMessage?chat_id" \
                     "=-1001782857066&text={}"



def convert_time(time_string, regex_time):
    time_string = unicodedata.normalize("NFKD", time_string)
    publish_date = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
    date_now = datetime.now()
    if unicodedata.normalize("NFKD", "giờ trước") in time_string:
        hours_value = None
        list_ele = time_string.split(' ')
        for element in list_ele:
            if hours_value is None:
                try:
                    hours_value = int(element)
                    break
                except:
                    pass
        try:
            publish_date = (datetime.now() - timedelta(hours=hours_value)).strftime('%Y-%m-%dT%H:%M:%SZ')
            return publish_date
        except:
            pass
    if unicodedata.normalize("NFKD", "phút trước") in time_string:
        minutes_value = None
        list_element = time_string.split(' ')
        for element in list_element:
            if minutes_value is None:
                try:
                    minutes_value = int(element)
                    break
                except:
                    pass

        if minutes_value is not None:
            return (datetime.now() - timedelta(minutes=minutes_value)).strftime('%Y-%m-%dT%H:%M:%SZ')

    temp = None
    if "/" in regex_time:
        temp = "/"
    elif "-" in regex_time:
        temp = "-"
    try:
        time_string = time_string.replace(u'\xa0', u' ')
        time_string = time_string.replace(u'\t', u' ')
        time_split = time_string.split(' ')

        date_define_value = None
        hour_define_value = None

        for t in time_split:
            try:
                if temp in t and len(t) > 3:
                    t = re.sub('[^0-9./-]', '', t)
                    date_define_value = t
                if ":" in t and hour_define_value is None and len(re.findall('[0-9]+', t)) > 0:
                    t = re.sub('[^0-9.:]', '', t)
                    hour_define_value = t
            except:
                pass
        if date_define_value is not None:
            date_define = date_define_value.replace(',', '')
            try:
                hour_define_value = hour_define_value.replace(',', '')
            except:
                pass
            if hour_define_value is not None:
                if hour_define_value.count(":") == 1:
                    hours_define = (hour_define_value + ":00").replace(',', '')

                elif hour_define_value.count(":") == 3:
                    hours_define = hour_define_value[:hour_define_value.rindex(":")]
                else:
                    hours_define = hour_define_value
                date_time_str = date_define + " " + hours_define
                if len(date_define_value.split(temp)[-1]) == 2:
                    try:
                        date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%y %H:%M:%S')
                    except:
                        date_obj = datetime.fromisoformat(date_time_str)
                else:
                    try:
                        date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
                    except:
                        date_obj = datetime.fromisoformat(date_time_str)


            else:
                date_time_str = date_define + " " + str(date_now.hour) + ":" + str(date_now.minute) + ":" + str(
                    date_now.second)
                date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
            publish_date = date_obj.strftime('%Y-%m-%dT%H:%M:%SZ')
        elif hour_define_value is not None:
            date_time_str = str(date_now.day)+temp+str(date_now.month)+temp+str(date_now.year)+ " "
            if hour_define_value.count(":") == 1:
                hours_define = (hour_define_value + ":00").replace(',', '')

            elif hour_define_value.count(":") == 3:
                hours_define = hour_define_value[:hour_define_value.rindex(":")]
            else:
                hours_define = hour_define_value
            date_obj = datetime.strptime(date_time_str+hours_define, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
            publish_date = date_obj.strftime('%Y-%m-%dT%H:%M:%SZ')
    except:
        pass
    return publish_date


def convert_content(content_general, message):
    full_contents = []
    attachment_urls = []

    if len(content_general) == 1:
        content_general = content_general[0].contents

    for content in content_general:
        try:
            _class = content.attrs[
                'class'][0]

            if _class == message['textXpath'].replace('\'', '"'):
                full_contents.append(
                    FullContent(content=content.text, id=str(content.text).__hash__(), type="text").get_dict())
            elif _class == message['imageXpath'].replace('\'', '"'):
                imgs = content.contents
                for img in imgs:
                    temp = 0
                    try:
                        try:
                            try:
                                url_img = img.attrs['content']
                            except:
                                try:
                                    url_img = img.contents[0].attrs['data-original']
                                except:
                                    url_img = \
                                    str(content.findParent)[str(content.findParent).index("src") + 5:].split('"')[0]

                            if not url_img.startswith("http") and len(url_img) > 0:
                                url_img = message['linkDomain'] + url_img

                            if len(url_img) == 0:
                                try:
                                    url_img= str(content.findParent)[str(content.findParent).index("data-src") + 10:].split('"')[0]
                                except:
                                    pass

                            if len(url_img) > 0:
                                full_contents.append(FullContent(content=url_img, id=str(url_img).__hash__(),
                                                                 type="img").get_dict())
                                attachment_urls.append(
                                    AttachmentUrl(url=url_img, id=str(url_img).__hash__(), type="img").get_dict())
                                temp = 1
                        except:
                            for img_path in str(content.findParent)[
                                            str(content.findParent).index("src") + 5:].split('"'):
                                if (".jpg" in img_path or ".png" in img_path or ".jpeg" in img_path) and img_path.startswith("http"):
                                    full_contents.append(
                                        FullContent(content=img_path, id=str(img_path).__hash__(),
                                                    type="img").get_dict())
                                    attachment_urls.append(
                                        AttachmentUrl(url=img_path, id=str(img_path).__hash__(),
                                                      type="img").get_dict())
                                    temp = 1
                                    break
                        if temp == 1:
                            break
                    except:
                        continue
        except:
            str_data = str(content.findParent)
            try:
                url_img = str_data[str_data.index("src=") + 5:].split('"')[0]
                if not url_img.startswith("http"):
                    url_img = message['linkDomain'] + url_img
                full_contents.append(FullContent(content=url_img, id=str(url_img).__hash__(), type="img").get_dict())
                attachment_urls.append(AttachmentUrl(url=url_img, id=str(url_img).__hash__(), type="img").get_dict())

                if len(content.contents) >1:
                    full_contents.append(FullContent(content=str(content.contents[1]), id=str(content.contents[1]).__hash__(), type="text").get_dict())
            except:
                try:
                    if len(content.text) > 6:
                        full_contents.append(
                            FullContent(content=content.text, id=str(content.text).__hash__(), type="text").get_dict())
                except:
                    try:
                        if len(str(content)) > 6:
                            full_contents.append(
                                FullContent(content=str(content), id=str(content).__hash__(), type="text").get_dict())
                    except:
                        pass


    return full_contents, attachment_urls


def convert_author(message, response_detail):
    author = message['nameDomain']
    author_general = message['authorXpath'].replace('\'', '"').split('|')
    try:
        author = BeautifulSoup(response_detail, "lxml").find(author_general[0],
                                                             {author_general[1]: author_general[2]}).text

        if len(author) > 30 > len(BeautifulSoup(response_detail, "lxml").findAll(author_general[0], {
            author_general[1]: author_general[2]})[-1].text):
            author = BeautifulSoup(response_detail, "lxml").findAll(author_general[0],
                                                                    {author_general[1]: author_general[2]})[
                -1].text
        elif len(author) > 30 > len(BeautifulSoup(response_detail, "lxml").findAll(author_general[0],
                                                                                   {author_general[1]: author_general[
                                                                                       2]})[0].contents[-1].text):
            author = BeautifulSoup(response_detail, "lxml").findAll(author_general[0],
                                                                    {author_general[1]: author_general[2]})[0].contents[
                -1].text
    except:
        pass

    if len(author) < 2:
        author = message['nameDomain']

    return author


def convert_publish_date(response_detail, message):
    time_string = None
    try:
        time_string = html.fromstring(response_detail).xpath(message['timeXpath'].replace('\'', '"'))[0].text
        # time_string = html.fromstring(data_detail.text).xpath("//time[@class='cate-24h-foot-arti-deta-cre-post']")[0].text
    except:
        try:
            time_string = html.fromstring(response_detail).cssselect(message['timeXpath'].replace('\'', '"'))[0].text
        except:
            pass
    if time_string is None:
        try:
            time_string = html.fromstring(response_detail).xpath(message['timeXpath'].replace('\'', '"'))[0].tail
        except:
            try:
                time_string = str(html.fromstring(response_detail).xpath(message['timeXpath'].replace('\'', '"'))[0])
                return datetime.fromisoformat(time_string).strftime('%Y-%m-%dT%H:%M:%SZ')
            except:
                pass

    publish_date = convert_time(time_string.replace("\r", "").strip().replace("\n", ""),
                                message['timeRegex'].replace('\'', '"'))

    return publish_date


def convert_top_content(response_detail, message):
    title = html.fromstring(response_detail).xpath(message['titleXpath'].replace('\'', '"'))[0].text
    description = None
    try:
        description = html.fromstring(response_detail).xpath(message['descriptionXpath'].replace('\'', '"'))[0].text
    except:
        pass

    return title, description


def convert_url(url, headers, message):
    data_detail = requests.get(url=url, headers=headers, verify=False)
    data_detail.encoding = data_detail.apparent_encoding
    response_detail = data_detail.text

    list_content_general = message['contentParentXpath'].replace('\'', '"').split("|")
    if len(list_content_general) == 4:
        response_detail = data_detail.text

    content_general = BeautifulSoup(response_detail, "lxml").find(list_content_general[0], {
        list_content_general[1]: list_content_general[2]}).contents

    return response_detail, content_general


def convert_message(message, headers):
    list_link = []

    response = requests.get(message['cateLink'], headers=headers, verify=False)

    list_a = html.fromstring(response.text).xpath(message.get("linksXpath").replace('\'', '"'))
    while len(list_a) == 0:
        if "Yêu cầu của bạn hệ thống chưa kịp xử lí hoặc nội dung không tìm thấy !" in response.text:
            response = requests.get(message['cateLink'], headers=headers, verify=False)
            data = response.text

            list_a = html.fromstring(data).xpath(message.get("linksXpath").replace('\'', '"'))

        else:
            list_a.append("website")

    for href in list_a:
        try:
            if href.get("href") in list_link:
                continue
            list_link.append(href.get("href"))
        except:
            pass

    return list_link


def run(message):
    list_data = []
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36'}

    list_link = convert_message(message=message, headers=headers)

    for url in list_link:
        try:
            if url.startswith("http") and message['nameDomain'] not in url:
                continue
            if not url.startswith("http"):
                url = message['linkDomain'].replace('\'', '"') + url

            response_detail, content_general = convert_url(url=url, headers=headers, message=message)
            author = convert_author(message=message, response_detail=response_detail)

            title, description = convert_top_content(response_detail=response_detail, message=message)

            publish_date = convert_publish_date(response_detail=response_detail, message=message)

            full_contents, attachment_urls = convert_content(content_general=content_general, message=message)

            special_fields = NewsSpecialFields(None, None, author, None)
            bean_data = NewsBeanContent(bean_type=BeanType.bean_content_news,
                                        download_date=datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ'),
                                        full_contents=full_contents, publish_date=publish_date, title=title,
                                        description=description, url=url, attachment_urls=attachment_urls,
                                        domain=message['nameDomain'],
                                        special_fields=special_fields, origin=0, source_name=author, source_id=None)

            list_data.append(bean_data.get_dict())
        except:
            continue

    return list_data



if __name__ == '__main__':
    message_nbtv = {
        "id": 548,
        "cateLink": "https://nbtv.vn/chinh-tri",
        "linksXpath": "//a[@class='title']",
        "titleXpath": "//span[@class='title']",
        "descriptionXpath": "//div[@class='desc']",
        "contentParentXpath": "div|class|fr-view info",
        "textXpath": "",
        "imageXpath": "",
        "timeXpath": "//div[@class='date']",
        "timeRegex": "%d.%m.%Y %H:%M ",
        "nameDomain": "nbtv.vn",
        "linkDomain": "https://nbtv.vn",
        "authorXpath": "p|style|text-align: right;"
    }
    list_data = run(message=message_nbtv)
    print("1")


