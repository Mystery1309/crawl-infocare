from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from time import sleep
from datetime import datetime, timedelta
import unicodedata
import re



from idhs.common.model.base_object import FullContent, AttachmentUrl, BeanType
from idhs.module_forum.model.forums_object import ForumsSpecialFields, ForumsBeanContent


def customChrome(executable_path):
    option = Options()
    option.add_argument("--enable-extentions")
    # option.add_argument('headless')
    option.add_argument("--no-sandbox")

    driver = webdriver.Chrome(chrome_options=option, executable_path=executable_path)
    driver.implicitly_wait(3)
    driver.maximize_window()

    return driver


def convertLinkPost(executable_path, message):
    browser = customChrome(executable_path)
    browser.get(message['linkCate'])

    linksPost = []

    linksPostXpath = browser.find_elements_by_xpath(message['linksPostXpath'])

    for linkXpath in linksPostXpath:
        linksPost.append(linkXpath.get_attribute("href"))

    browser.close()

    return linksPost


def convertContent(message, parentContent):
    attachments_url, full_contents = [], []

    try:
        full_contents.append(FullContent(id=str(parentContent.text).__hash__(), content=parentContent.text, type="text").get_dict())

        listImageXpath = parentContent.find_elements_by_css_selector(message['imageXpath'])

        attachments_url = [AttachmentUrl(id=str(imageXpath.get_attribute("src")).__hash__(), url=imageXpath.get_attribute("src"), type="img").get_dict() for imageXpath in listImageXpath]
    except:
        pass

    return attachments_url, full_contents


def convertTime(timeString, message):
    time_string = unicodedata.normalize("NFKD", timeString)
    publish_date = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
    date_now = datetime.now()
    if unicodedata.normalize("NFKD", "giờ trước") in time_string:
        hours_value = None
        list_ele = time_string.split(' ')
        for element in list_ele:
            if hours_value is None:
                if unicodedata.normalize("NFKD", "một") in time_string:
                    hours_value = 1
                    break
                else:
                    try:
                        hours_value = int(element)
                        break
                    except:
                        pass
        try:
            publish_date = (datetime.now() - timedelta(hours=hours_value)).strftime('%Y-%m-%dT%H:%M:%SZ')
            return publish_date
        except:
            pass
    if unicodedata.normalize("NFKD", "phút trước") in time_string:
        minutes_value = None
        list_element = time_string.split(' ')
        for element in list_element:
            if minutes_value is None:
                if unicodedata.normalize("NFKD", "một") in time_string:
                    minutes_value = 1
                    break
                else:
                    try:
                        minutes_value = int(element)
                        break
                    except:
                        pass

        if minutes_value is not None:
            return (datetime.now() - timedelta(minutes=minutes_value)).strftime('%Y-%m-%dT%H:%M:%SZ')


    if unicodedata.normalize("NFKD", "ngày trước") in time_string:
        day_value = None
        list_element = time_string.split(' ')
        for element in list_element:
            if day_value is None:
                if unicodedata.normalize("NFKD", "một") in time_string:
                    day_value = 1
                    break
                else:
                    try:
                        day_value = int(element)
                        break
                    except:
                        pass

        if day_value is not None:
            return (datetime.now() - timedelta(days=day_value)).strftime('%Y-%m-%dT%H:%M:%SZ')

    if unicodedata.normalize("NFKD", "tháng trước") in time_string:
        month_value = None
        list_element = time_string.split(' ')
        for element in list_element:
            if month_value is None:
                if unicodedata.normalize("NFKD", "một") in time_string:
                    month_value = 1
                    break
                else:
                    try:
                        month_value = int(element)
                        break
                    except:
                        pass

        if month_value is not None:
            return (datetime.now() - timedelta(days=month_value*30)).strftime('%Y-%m-%dT%H:%M:%SZ')

    temp = None
    if "/" in message['timeRegex']:
        temp = "/"
    elif "-" in message['timeRegex']:
        temp = "-"
    try:
        time_string = time_string.replace(u'\xa0', u' ')
        time_string = time_string.replace(u'\t', u' ')
        time_split = time_string.split(' ')

        date_define_value = None
        hour_define_value = None

        for t in time_split:
            try:
                if temp in t and len(t) > 3:
                    t = re.sub('[^0-9./-]', '', t)
                    date_define_value = t
                if ":" in t and hour_define_value is None and len(re.findall('[0-9]+', t)) > 0:
                    t = re.sub('[^0-9.:]', '', t)
                    hour_define_value = t
            except:
                pass
        if date_define_value is not None:
            date_define = date_define_value.replace(',', '')
            try:
                hour_define_value = hour_define_value.replace(',', '')
            except:
                pass
            if hour_define_value is not None:
                if hour_define_value.count(":") == 1:
                    hours_define = (hour_define_value + ":00").replace(',', '')

                elif hour_define_value.count(":") == 3:
                    hours_define = hour_define_value[:hour_define_value.rindex(":")]
                else:
                    hours_define = hour_define_value
                date_time_str = date_define + " " + hours_define
                if len(date_define_value.split(temp)[-1]) == 2:
                    try:
                        date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%y %H:%M:%S')
                    except:
                        date_obj = datetime.fromisoformat(date_time_str)
                else:
                    try:
                        date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
                    except:
                        date_obj = datetime.fromisoformat(date_time_str)


            else:
                date_time_str = date_define + " " + str(date_now.hour) + ":" + str(date_now.minute) + ":" + str(
                    date_now.second)
                date_obj = datetime.strptime(date_time_str, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
            publish_date = date_obj.strftime('%Y-%m-%dT%H:%M:%SZ')
        elif hour_define_value is not None:
            date_time_str = str(date_now.day) + temp + str(date_now.month) + temp + str(date_now.year) + " "
            if hour_define_value.count(":") == 1:
                hours_define = (hour_define_value + ":00").replace(',', '')

            elif hour_define_value.count(":") == 3:
                hours_define = hour_define_value[:hour_define_value.rindex(":")]
            else:
                hours_define = hour_define_value
            date_obj = datetime.strptime(date_time_str + hours_define, '%d' + temp + '%m' + temp + '%Y %H:%M:%S')
            publish_date = date_obj.strftime('%Y-%m-%dT%H:%M:%SZ')
    except:
        pass
    return publish_date


def convertReaction(browser):
    reactionCount, likeCount, commentCount = 0, 0, 0

    reactions = browser.find_element_by_xpath(message['reactionXpath']).text

    try:
        if 'K' in reactions:
            reactionCount = int(float(reactions.split("\n")[0].replace('K', '')) * 1000)
        else:
            reactionCount = int(reactions.split("\n")[0])
    except:
        pass

    try:
        likeCount = int(reactions.split("\n")[1])
    except:
        pass

    try:
        commentCount = int(reactions.split("\n")[2])
    except:
        pass


    return reactionCount, likeCount, commentCount


def convertAuthor(browser, message):
    authorLink, authorName = None, None

    authorXpath = browser.find_element_by_xpath(message['authorXpath'])

    authorLink = authorXpath.get_attribute("href")

    authorName = authorXpath.text

    return authorLink, authorName


def run(executable_path, message):

    list_data = []

    linksPost = list(set(list(filter(None, convertLinkPost(executable_path, message)))))

    for url in linksPost:
        browser = customChrome(executable_path)
        try:
            browser.get(url)
            title = browser.find_element_by_xpath(message['titleXpath']).text

            parentContent = browser.find_element_by_xpath(message['contentParentXpath'])

            attachment_urls, full_contents = convertContent(message, parentContent)

            if len(full_contents) == 0:
                continue

            timeString = browser.find_element_by_xpath(message['timeXpath']).text

            publishDate = convertTime(timeString, message)

            reactionCount, likeCount, commentCount = convertReaction(browser)

            authorLink, authorName = convertAuthor(browser, message)

            author_id = authorLink.split("/")[-1] if authorLink is not None else message['nameDomain']

            from_object = {"id": author_id, "author": authorName, "author_link": authorLink}
            to_object = {"id": message['linkCate'].split("/")[-1], "author": message['nameCate'],
                         "author_link": message['linkCate']}
            forum_special_fields = ForumsSpecialFields(comment_count=commentCount, reaction_count=reactionCount,
                                                       like=likeCount, dislike=0,
                                                       id=url.split("/")[-1], from_obj=from_object,
                                                       to_obj=to_object)
            data_dict = ForumsBeanContent(bean_type=BeanType.bean_content_forum_post,
                                          download_date=datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
                                          full_contents=full_contents, publish_date=publishDate, title=title,
                                          description=title,
                                          url=url, attachment_urls=attachment_urls, domain=message['linkDomain'],
                                          special_fields=forum_special_fields,
                                          origin=0, source_id=from_object['id'], source_name=from_object['author'],
                                          hastags=[]).get_dict()

            list_data.append(data_dict)
        except:
            pass

        browser.close()



    return list_data


if __name__ == '__main__':
    message = {
        "linkCate": "https://www.webtretho.com/f/bat-dong-san-nha-dat",
        "linksPostXpath": "//div[@class='am-card post-item']//div[@class='am-card-body py__0 px__0 post-item__content']//div//a",
        "timeXpath": "//div[@class='avatar-info']//div",
        "authorXpath": "//div[@class='avatar-info']//a",
        "titleXpath": "//h1",
        "contentParentXpath": "//div[@id='post-content']",
        "nameDomain": "webtretho.com",
        "linkDomain": "https://www.webtretho.com",
        "imageXpath": "img",
        "contentXpath": "",
        "reactionXpath": "//div[@class='jsx-2059343162 post-view']//div[@class='am-flexbox am-flexbox-align-center']//div[@class='am-flexbox am-flexbox-align-center']",
        "timeRegex": "",
        "nameCate": "Bất động sản - Nhà đất"
    }

    message1 = {
        "linkCate": "https://www.webtretho.com/f/y-tuong-kinh-doanh-khoi-nghiep",
        "linksPostXpath": "//div[@class='am-wingblank am-wingblank-lg']//a",
        "timeXpath": "//div[@class='avatar-info']//div",
        "authorXpath": "//div[@class='avatar-info']//a",
        "titleXpath": "//h1",
        "contentParentXpath": "//div[@id='post-content']",
        "nameDomain": "webtretho.com",
        "linkDomain": "https://www.webtretho.com",
        "imageXpath": "img",
        "contentXpath": "",
        "reactionXpath": "//div[@class='jsx-2059343162 post-view']//div[@class='am-flexbox am-flexbox-align-center']//div[@class='am-flexbox am-flexbox-align-center']",
        "timeRegex": "",
        "nameCate": "Ý tưởng kinh doanh, khởi nghiệp"
    }

    message2 = {
        "linkCate": "https://www.webtretho.com/f/hoc-tap-va-nghien-cuu",
        "linksPostXpath": "//div[@class='am-wingblank am-wingblank-lg']//a",
        "timeXpath": "//div[@class='avatar-info']//div",
        "authorXpath": "//div[@class='avatar-info']//a",
        "titleXpath": "//h1",
        "contentParentXpath": "//div[@id='post-content']",
        "nameDomain": "webtretho.com",
        "linkDomain": "https://www.webtretho.com",
        "imageXpath": "img",
        "contentXpath": "",
        "reactionXpath": "//div[@class='jsx-2059343162 post-view']//div[@class='am-flexbox am-flexbox-align-center']//div[@class='am-flexbox am-flexbox-align-center']",
        "timeRegex": "",
        "nameCate": "Học tập và nghiên cứu"
    }

    message3 = {
        "linkCate": "https://www.webtretho.com/f/thong-tin-giao-duc",
        "linksPostXpath": "//div[@class='am-wingblank am-wingblank-lg']//a",
        "timeXpath": "//div[@class='avatar-info']//div",
        "authorXpath": "//div[@class='avatar-info']//a",
        "titleXpath": "//h1",
        "contentParentXpath": "//div[@id='post-content']",
        "nameDomain": "webtretho.com",
        "linkDomain": "https://www.webtretho.com",
        "imageXpath": "img",
        "contentXpath": "",
        "reactionXpath": "//div[@class='jsx-2059343162 post-view']//div[@class='am-flexbox am-flexbox-align-center']//div[@class='am-flexbox am-flexbox-align-center']",
        "timeRegex": "",
        "nameCate": "Thông tin giáo dục"
    }

    message4 = {
        "linkCate": "https://www.webtretho.com/f/chuyen-goc-bep",
        "linksPostXpath": "//div[@class='am-wingblank am-wingblank-lg']//a",
        "timeXpath": "//div[@class='avatar-info']//div",
        "authorXpath": "//div[@class='avatar-info']//a",
        "titleXpath": "//h1",
        "contentParentXpath": "//div[@id='post-content']",
        "nameDomain": "webtretho.com",
        "linkDomain": "https://www.webtretho.com",
        "imageXpath": "img",
        "contentXpath": "",
        "reactionXpath": "//div[@class='jsx-2059343162 post-view']//div[@class='am-flexbox am-flexbox-align-center']//div[@class='am-flexbox am-flexbox-align-center']",
        "timeRegex": "",
        "nameCate": "Chuyện góc bếp"
    }

    message5 = {
        "linkCate": "https://www.webtretho.com/f/sao-lam-dep",
        "linksPostXpath": "//div[@class='am-wingblank am-wingblank-lg']//a",
        "timeXpath": "//div[@class='avatar-info']//div",
        "authorXpath": "//div[@class='avatar-info']//a",
        "titleXpath": "//h1",
        "contentParentXpath": "//div[@id='post-content']",
        "nameDomain": "webtretho.com",
        "linkDomain": "https://www.webtretho.com",
        "imageXpath": "img",
        "contentXpath": "",
        "reactionXpath": "//div[@class='jsx-2059343162 post-view']//div[@class='am-flexbox am-flexbox-align-center']//div[@class='am-flexbox am-flexbox-align-center']",
        "timeRegex": "",
        "nameCate": "Chuyện góc bếp"
    }

    list_data = run(executable_path="etc/chromedriver", message=message5)
    print("1")